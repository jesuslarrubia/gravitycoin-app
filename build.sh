#!/bin/bash

CWD="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

# Show the help text
function show_help() {
  echo ""
  echo "Usage: sh build.sh <command> [<args>]"
  echo ""
  echo "Meteor build command list:"
  echo "  --help|-h         Shows this help text."
  echo "  --build-deploy    Commits and pushes changes the to the current repository"
  echo "                    before deploying it to either the 'staging' or 'master'"
  echo "                    AWS OpsWork deployment."
  echo "  --build           Builds the Meteor bundle."
  echo "  --deploy          Deploys to the current branches deployment stack."
  echo ""
  exit 1
}


# Builds the Meteor bundle
function build_meteor_bundle() {

  # Remove old bundle
  rm -Rf cd ${CWD}/bundle/

  # Bundle up the Meteor project
  cd ${CWD}/src/
  npm install --production
  meteor build ../ --directory --architecture=os.linux.x86_64

  # Return back to current working directory root before continuing
  cd ${CWD}

  # Deploy only if a variable has been passed
  if [ -n "$1" ]; then
    aws_deploy
  fi
}


# AWS OpsWorks Deploy script
# Commits
function aws_deploy() {
  local AWS_REGION="eu-west-2"
  local AWS_CMD="{\"Name\": \"deploy\"}"
  local GIT_BRANCH=$(git branch 2>/dev/null| sed -n '/^\*/s/^\* //p')


  if [ "${GIT_BRANCH}" == "master" ]; then

    git add -A
    git commit -m "Created Meteor build bundle for deploying on the AWS OpsWorks 'master' stack."
    git push origin master

    # Deploy - eu-west-2 (London) cluster
    aws opsworks --region "${AWS_REGION}" create-deployment \
    --stack-id "0e78a17c-68ce-490e-b33e-104e1ae97b81" \
    --app-id "1bf43519-54fd-4f4e-8bfa-58ecc8ae8517" \
    --command "${AWS_CMD}"

  else
    echo "You can only deploy if you are on either the \"staging\" or \"master\" branches."
    exit 1
  fi
}

function check_node_version() {
  echo "Checking node version";
  echo ""

  nv=$(node --version)

  echo Current node version is: $nv
  echo ""

  case $nv in
    "v8.8.1"*)
    echo "Node version is correct. Proceeding with build."
    echo ""
    ;;
    *)
    echo "###################################################################################"
    echo "Incorrect node version. Halting build. Please switch to v4.8.3 in order to proceed."
    echo "###################################################################################"
    echo ""
    echo "For managing multiple node versions, we recommend NVM. More info at (https://github.com/creationix/nvm)"
    
    exit 1
    ;;
  esac
}

# Show the help text if no parameters have been given
if test $# -eq 0; then
  show_help
else

  # Flags and their functionalities
  while test $# -gt 0; do
    case "$1" in
      --help|-h)
        show_help
        ;;
      --build-deploy)
        check_node_version
        build_meteor_bundle "true"
        shift
        ;;
      --build)
        check_node_version
        build_meteor_bundle
        shift
        ;;
      --deploy)
        aws_deploy
        shift
        ;;
      *)
        if test $# -gt 0; then
          show_help
        fi
        break
        ;;
    esac
  done
fi
