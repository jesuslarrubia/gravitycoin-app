/* Imports for global scope */

MongoInternals = Package.mongo.MongoInternals;
Mongo = Package.mongo.Mongo;
ReactiveVar = Package['reactive-var'].ReactiveVar;
Tracker = Package.tracker.Tracker;
Deps = Package.tracker.Deps;
ECMAScript = Package.ecmascript.ECMAScript;
FlowRouter = Package['kadira:flow-router'].FlowRouter;
BlazeLayout = Package['kadira:blaze-layout'].BlazeLayout;
check = Package.check.check;
Match = Package.check.Match;
ReactiveMethod = Package['simple:reactive-method'].ReactiveMethod;
Meteor = Package.meteor.Meteor;
global = Package.meteor.global;
meteorEnv = Package.meteor.meteorEnv;
WebApp = Package.webapp.WebApp;
WebAppInternals = Package.webapp.WebAppInternals;
main = Package.webapp.main;
_ = Package.underscore._;
DDP = Package['ddp-client'].DDP;
DDPServer = Package['ddp-server'].DDPServer;
LaunchScreen = Package['launch-screen'].LaunchScreen;
Blaze = Package.ui.Blaze;
UI = Package.ui.UI;
Handlebars = Package.ui.Handlebars;
Spacebars = Package.spacebars.Spacebars;
meteorInstall = Package.modules.meteorInstall;
meteorBabelHelpers = Package['babel-runtime'].meteorBabelHelpers;
Promise = Package.promise.Promise;
Accounts = Package['accounts-base'].Accounts;
Autoupdate = Package.autoupdate.Autoupdate;
HTML = Package.htmljs.HTML;

