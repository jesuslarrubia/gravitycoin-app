var require = meteorInstall({"api":{"eth":{"common":{"collections":{"collections.js":function(require,exports,module){

//////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                          //
// api/eth/common/collections/collections.js                                                                //
//                                                                                                          //
//////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                            //
module.export({
  Ethaccounts: () => Ethaccounts,
  Ethtransfers: () => Ethtransfers
});
let Mongo;
module.watch(require("meteor/mongo"), {
  Mongo(v) {
    Mongo = v;
  }

}, 0);
let Ethaccounts = new Mongo.Collection("ethaccounts");
let Ethtransfers = new Mongo.Collection("ethtransfers");
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

}}},"server":{"methods":{"ethaccounts.js":function(require,exports,module){

//////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                          //
// api/eth/server/methods/ethaccounts.js                                                                    //
//                                                                                                          //
//////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                            //
var _extends = require("@babel/runtime/helpers/builtin/extends");

let Meteor;
module.watch(require("meteor/meteor"), {
  Meteor(v) {
    Meteor = v;
  }

}, 0);
let Web3jsWrapper;
module.watch(require("../models/Web3jsWrapper"), {
  Web3jsWrapper(v) {
    Web3jsWrapper = v;
  }

}, 1);
let Match;
module.watch(require("meteor/check"), {
  Match(v) {
    Match = v;
  }

}, 2);
let Ethaccounts, Ethtransfers;
module.watch(require("../../common/collections/collections"), {
  Ethaccounts(v) {
    Ethaccounts = v;
  },

  Ethtransfers(v) {
    Ethtransfers = v;
  }

}, 3);
Meteor.methods({
  /**
   * Create an account in the Ethereum netowrk for an user.
   */
  createAccount: email => {
    check(email, String);
    const user = Meteor.users.findOne({
      "emails.address": email
    });

    if (!user) {
      throw new Error(`ERROR creating Eth account - User not found: ${email}`);
    } else if (user._id !== Meteor.userId()) {
      return {
        error: 1,
        message: `ERROR creating Eth account - You are not allowed to use this user.`
      };
    }

    let web3 = new Web3jsWrapper();
    const account = web3.createAccount(user._id);

    if (account.error) {
      return {
        error: 1,
        message: account.message
      };
    }

    return {
      message: "The account was created successfully in the Ethereum network!."
    };
  },

  /**
   * Associate an user to an existing Ethereum account.
   * 
   * @param {String} email - User's email. It must already exist in the platform.
   * @param {String} account - Ethereum account.
   */
  associateAccount: (email, account) => {
    check(email, String);
    const user = Meteor.users.findOne({
      "emails.address": email
    });

    if (!user) {
      return {
        error: 1,
        message: `ERROR creating Eth account - User not found: ${email}`
      };
    } else if (user._id !== Meteor.userId()) {
      return {
        error: 1,
        message: `ERROR creating Eth account - You are not allowed to use this user.`
      };
    }

    let web3 = new Web3jsWrapper();
    const result = web3.associateAccount(user._id, account);

    if (result.error) {
      return {
        error: 1,
        message: result.message
      };
    }

    return {
      message: "The account was created successfully associated!."
    };
  },

  /**
   * Get the balance of an user.
   *
   * @param {String} userId - The user id.
   * 
   * @returns {Number} - The account balance.
   */
  getUserBalance: userId => {
    check(userId, String);
    const account = Ethaccounts.findOne({
      userId
    });

    if (!account) {
      throw new Error(`ERROR retrieving user balance: Not found account for userId: ${userId}`);
    }

    let web3 = new Web3jsWrapper();

    try {
      return web3.getAccountBalance(account.address);
    } catch (error) {
      return {
        error: 1,
        message: error
      };
    }
  },

  /**
   * Decrypts a keystore v3 JSON returning its account.
   *
   * @param {*} keyStoreFile - The encrypted private key to decrypt.
   * @param {*} keyStorePassword - The password used for encryption.
   * 
   * @returns {Object} = The Ethereum account.
   */
  loadAccountFromKeystore: (keyStoreFile, keyStorePassword) => {
    check(keyStoreFile, String);
    check(keyStorePassword, String);
    let web3 = new Web3jsWrapper();

    try {
      return web3.getWeb3Instance().eth.accounts.decrypt(keyStoreFile, keyStorePassword);
    } catch (error) {
      return {
        error: 1,
        message: `ERROR loading account from Keystore: ${error.message}`
      };
    }
  },

  /**
   * Creates an account object from a private key.
   *
   * @param {String} privateKey - The account private key.
   * 
   * @returns {Object} = The Ethereum account.
   */
  loadAccountFromPrivateKey: privateKey => {
    check(privateKey, String);
    let web3 = new Web3jsWrapper();

    try {
      return web3.getWeb3Instance().eth.accounts.privateKeyToAccount(privateKey);
    } catch (error) {
      return {
        error: 1,
        message: `ERROR loading account from Private Key: ${error.message}`
      };
    }
  },

  /**
   * Transfer tokens between users.
   * 
   * @param {String} fromUser - sender user. It is required to have
   *  tokens and Ether in the current network.
   * @param {String} toUser - recipient account user.
   * @param {String|Number} amount - amount of tokens to send.
   * 
   * @returns {Promise<Object>} - The transaction receipt.
   */
  transfer: (fromUser, toUser, amount) => {
    check(fromUser, String);
    check(toUser, String);
    check(amount, Match.OneOf(String, Number));

    if (fromUser !== Meteor.userId()) {
      return {
        error: 1,
        message: `ERROR transfering token: You are not allowed to transfer from this user.`
      };
    }

    const fromAccount = Ethaccounts.findOne({
      userId: fromUser
    });
    const toAccount = Ethaccounts.findOne({
      userId: toUser
    });

    if (!fromAccount || !toAccount) {
      return {
        error: 1,
        message: `ERROR transfering token: Not found account for userId: ${fromUser} or ${toUser}`
      };
    }

    let web3 = new Web3jsWrapper();
    return web3.transferFrom(fromAccount.address, toAccount.address, parseInt(amount), fromAccount.privateKey).then(receipt => {
      const transfer = {
        status: 'success',
        from: receipt.from,
        to: toAccount.address,
        gasUsed: receipt.gasUsed,
        transactionHash: receipt.transactionHash,
        amount: amount,
        receipt: receipt
      };
      const transferId = Ethtransfers.insert(transfer);
      transfer._id = transferId;
      return transfer;
    }).catch(error => {
      const transfer = {
        status: 'error',
        error: error
      };
      const transferId = Ethtransfers.insert(transfer);
      transfer._id = transferId;
      return _extends({}, transfer, {
        error: 1
      });
    });
  }
});
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

}},"models":{"Web3jsWrapper.js":function(require,exports,module){

//////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                          //
// api/eth/server/models/Web3jsWrapper.js                                                                   //
//                                                                                                          //
//////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                            //
var _extends = require("@babel/runtime/helpers/builtin/extends");

module.export({
  Web3jsWrapper: () => Web3jsWrapper
});
let Web3;
module.watch(require("web3"), {
  default(v) {
    Web3 = v;
  }

}, 0);
let Meteor;
module.watch(require("meteor/meteor"), {
  Meteor(v) {
    Meteor = v;
  }

}, 1);
let Ethaccounts;
module.watch(require("../../common/collections/collections"), {
  Ethaccounts(v) {
    Ethaccounts = v;
  }

}, 2);

class Web3jsWrapper {
  /**
   * Web3jsWrapper.
   * 
   * Initialise Web3 object with platform-dependant parameters.
   * 
   * @param {*} httpProvider - Ethereum network.
   */
  constructor(httpProvider = Meteor.settings.NETWORK_URL) {
    this.web3 = new Web3(new Web3.providers.HttpProvider(httpProvider)); // ChainId is 3 for the test network ropsten. 

    this.chainId = Meteor.settings.NETWORK_CHAINID; // This is the contract deployed to the Eth network with the token info.

    this.tokenAddress = Meteor.settings.TOKEN_ADDRESS;
    let abiOfContract = Meteor.settings.TOKEN_ABI;
    abiOfContract = JSON.parse(abiOfContract);
    this.contract = new this.web3.eth.Contract(abiOfContract, this.tokenAddress);
  }
  /**
   * Get the Web3 instance.
   * 
   * @return {Object} - Web3 instance.  
   */


  getWeb3Instance() {
    return this.web3;
  }
  /**
   * Create an account in the ethereum network for an user.
   * 
   * @param {String} userId - Id of the user.
   *
   * @return {Object} - Created Ethereum account.  
   */


  createAccount(userId) {
    let account;

    try {
      account = this.web3.eth.accounts.create();
    } catch (error) {
      return {
        error: 1,
        message: `ERROR creating Ethereum account: ${error.message}`
      };
    }

    return this.associateAccount(userId, account);
  }
  /**
   * Associate an Ethereum account with an user.
   * 
   * @param {String} userId - Id of the user.
   *
   * @return {Object} - Created Ethereum account.
   */


  associateAccount(userId, account) {
    let accountData = {
      address: account.address,
      privateKey: account.privateKey,
      userId: userId
    };
    const existing = Ethaccounts.findOne({
      address: account.address
    });

    if (existing) {
      return {
        error: 1,
        message: `ERROR associating Ethereum account to the user: The account already exist`
      };
    }

    let accountId = Ethaccounts.insert(accountData);

    if (!accountId) {
      return {
        error: 1,
        message: `ERROR associating Ethereum account: The account couldn't be persisted in DB`
      };
    }

    return _extends({}, accountData, {
      _id: accountId
    });
  }

  catch(error) {
    return {
      error: 1,
      message: `ERROR associating Eth account: ${error.message}`
    };
  }
  /**
   * Get the balance of an account.
   *
   * @param {String} account - Account address.
   * 
   * @returns {Promise<number>} - The account balance.
   */


  getAccountBalance(account) {
    return new Promise((resolve, reject) => {
      this.contract.methods.balanceOf(account).call({
        from: this.tokenAddress
      }).then(data => {
        resolve(data);
      }).catch(error => {
        reject(error);
        return;
      });
    });
  }
  /**
   * Transfer tokens between accounts.
   * 
   * @param {String} fromAccount - sender account address. It is required to have
   *  tokens and Ether.
   * @param {String} toAccount - recipient account address.
   * @param {String} amount - tokens to send.
   * 
   * @returns {Promise<Object>} - The transaction receipt.
   */


  transferFrom(fromAccount, toAccount, amount, privateKey) {
    let gasLimit, gasPrice;
    return new Promise((resolve, reject) => {
      this.web3.eth.estimateGas({
        from: fromAccount,
        to: this.tokenAddress,
        chainId: this.chainId,
        data: this.contract.methods.transfer(toAccount, amount).encodeABI()
      }).then(estimatedGas => {
        gasLimit = estimatedGas * 2;
        return this.web3.eth.getGasPrice();
      }).then(price => {
        gasPrice = price;
        return this.web3.eth.getTransactionCount(fromAccount);
      }).then(count => {
        return this.web3.eth.accounts.privateKeyToAccount(privateKey).signTransaction({
          from: fromAccount,
          to: this.tokenAddress,
          value: '0x0',
          gasPrice: this.web3.utils.toHex(gasPrice),
          gasLimit: this.web3.utils.toHex(gasLimit),
          nonce: "0x" + count.toString(16),
          chainId: this.chainId,
          data: this.contract.methods.transfer(toAccount, amount).encodeABI()
        });
      }).then(transaction => {
        this.web3.eth.sendSignedTransaction(transaction.rawTransaction).on('receipt', receipt => {
          // console.log('Transfer transaction finished succesfully:', receipt);
          resolve(receipt);
        }).on('error', error => {
          // console.log('ERROR in transfer transaction:', error.message);
          reject(error.message);
        });
      }).catch(error => {
        reject(error.message);
      });
    });
  }

}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

}},"publications":{"ethaccounts.js":function(require,exports,module){

//////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                          //
// api/eth/server/publications/ethaccounts.js                                                               //
//                                                                                                          //
//////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                            //
let Meteor;
module.watch(require("meteor/meteor"), {
  Meteor(v) {
    Meteor = v;
  }

}, 0);
let Ethaccounts;
module.watch(require("../../common/collections/collections"), {
  Ethaccounts(v) {
    Ethaccounts = v;
  }

}, 1);
Meteor.publish('ethAccounts', function (query = {}) {
  return Ethaccounts.find(query, {
    fields: {
      _id: 1,
      address: 1,
      userId: 1
    }
  });
});
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

},"ethtransfers.js":function(require,exports,module){

//////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                          //
// api/eth/server/publications/ethtransfers.js                                                              //
//                                                                                                          //
//////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                            //
let Meteor;
module.watch(require("meteor/meteor"), {
  Meteor(v) {
    Meteor = v;
  }

}, 0);
let Ethtransfers;
module.watch(require("../../common/collections/collections"), {
  Ethtransfers(v) {
    Ethtransfers = v;
  }

}, 1);
Meteor.publish('ethTransfers', function (query = {}, fields = {}) {
  return Ethtransfers.find(query, {
    fields: fields
  });
});
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

}}}},"users":{"server":{"publications":{"users.js":function(require,exports,module){

//////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                          //
// api/users/server/publications/users.js                                                                   //
//                                                                                                          //
//////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                            //
let Meteor;
module.watch(require("meteor/meteor"), {
  Meteor(v) {
    Meteor = v;
  }

}, 0);
Meteor.publish('usersProfile', function (query = {}) {
  return Meteor.users.find(query, {
    fields: {
      username: 1,
      profile: 1
    }
  });
});
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

}}}}},"imports":{"startup":{"common":{"config.js":function(require,exports,module){

//////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                          //
// imports/startup/common/config.js                                                                         //
//                                                                                                          //
//////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                            //
module.export({
  loadConfig: () => loadConfig
});
let Meteor;
module.watch(require("meteor/meteor"), {
  Meteor(v) {
    Meteor = v;
  }

}, 0);

function loadConfig() {
  Meteor.settings.APP_NAME = "GravityCoin";
  Meteor.settings.TOKEN_ADDRESS = "0x0d4d156bfa3fc1e5508e501774001bd0e1f6c722";
  Meteor.settings.TOKEN_NAME = "GravityCoin";
  Meteor.settings.TOKEN_ABI = `[{"constant":true,"inputs":[],"name":"name","outputs":[{"name":"","type":"string"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"name":"spender","type":"address"},{"name":"tokens","type":"uint256"}],"name":"approve","outputs":[{"name":"success","type":"bool"}],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[],"name":"totalSupply","outputs":[{"name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"name":"from","type":"address"},{"name":"to","type":"address"},{"name":"tokens","type":"uint256"}],"name":"transferFrom","outputs":[{"name":"success","type":"bool"}],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[],"name":"decimals","outputs":[{"name":"","type":"uint8"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[],"name":"_totalSupply","outputs":[{"name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[{"name":"tokenOwner","type":"address"}],"name":"balanceOf","outputs":[{"name":"balance","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[],"name":"acceptOwnership","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[],"name":"owner","outputs":[{"name":"","type":"address"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[],"name":"symbol","outputs":[{"name":"","type":"string"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[{"name":"a","type":"uint256"},{"name":"b","type":"uint256"}],"name":"safeSub","outputs":[{"name":"c","type":"uint256"}],"payable":false,"stateMutability":"pure","type":"function"},{"constant":false,"inputs":[{"name":"to","type":"address"},{"name":"tokens","type":"uint256"}],"name":"transfer","outputs":[{"name":"success","type":"bool"}],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[{"name":"a","type":"uint256"},{"name":"b","type":"uint256"}],"name":"safeDiv","outputs":[{"name":"c","type":"uint256"}],"payable":false,"stateMutability":"pure","type":"function"},{"constant":false,"inputs":[{"name":"spender","type":"address"},{"name":"tokens","type":"uint256"},{"name":"data","type":"bytes"}],"name":"approveAndCall","outputs":[{"name":"success","type":"bool"}],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[{"name":"a","type":"uint256"},{"name":"b","type":"uint256"}],"name":"safeMul","outputs":[{"name":"c","type":"uint256"}],"payable":false,"stateMutability":"pure","type":"function"},{"constant":true,"inputs":[],"name":"newOwner","outputs":[{"name":"","type":"address"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"name":"tokenAddress","type":"address"},{"name":"tokens","type":"uint256"}],"name":"transferAnyERC20Token","outputs":[{"name":"success","type":"bool"}],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[{"name":"tokenOwner","type":"address"},{"name":"spender","type":"address"}],"name":"allowance","outputs":[{"name":"remaining","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[{"name":"a","type":"uint256"},{"name":"b","type":"uint256"}],"name":"safeAdd","outputs":[{"name":"c","type":"uint256"}],"payable":false,"stateMutability":"pure","type":"function"},{"constant":false,"inputs":[{"name":"_newOwner","type":"address"}],"name":"transferOwnership","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"inputs":[],"payable":false,"stateMutability":"nonpayable","type":"constructor"},{"payable":true,"stateMutability":"payable","type":"fallback"},{"anonymous":false,"inputs":[{"indexed":true,"name":"_from","type":"address"},{"indexed":true,"name":"_to","type":"address"}],"name":"OwnershipTransferred","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"name":"from","type":"address"},{"indexed":true,"name":"to","type":"address"},{"indexed":false,"name":"tokens","type":"uint256"}],"name":"Transfer","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"name":"tokenOwner","type":"address"},{"indexed":true,"name":"spender","type":"address"},{"indexed":false,"name":"tokens","type":"uint256"}],"name":"Approval","type":"event"}]`;
  Meteor.settings.MASTER_ACCOUNT = "0x0de2f8c965126529c527d20598f1aa19ee83acec"; // Master account just will need one of these ones:
  // - Keystore data.

  Meteor.settings.MASTER_ACCOUNT_KEYSTORE_FILE = "UTC--2018-04-25T14-56-43.147751000Z--c5f5f7103431ccc1531b900cf70a7da863522520";
  Meteor.settings.MASTER_ACCOUNT_PASSWORD_FILE = "UTC--2018-04-25T14-56-43.147751000Z--c5f5f7103431ccc1531b900cf70a7da863522520_password"; // - Private key.
  // Must be preceded by 0x.

  Meteor.settings.MASTER_ACCOUNT_PRIVATEKEY_FILE = "../../../privateKeys/myPrivateKey";
  Meteor.settings.TOKEN_SYMBOL = "GTC";
  Meteor.settings.NETWORK_URL = "https://ropsten.infura.io"; // ChainId is 3 for the test network ropsten. 

  Meteor.settings.NETWORK_CHAINID = 3;
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

}},"server":{"config.js":function(require,exports,module){

//////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                          //
// imports/startup/server/config.js                                                                         //
//                                                                                                          //
//////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                            //
module.export({
  loadServerConfig: () => loadServerConfig
});
let loadConfig;
module.watch(require("../common/config"), {
  loadConfig(v) {
    loadConfig = v;
  }

}, 0);

function loadServerConfig(env) {
  switch (env) {
    case "production":
    case "staging":
    case "development":
    default:
      loadConfig();
      break;
  }
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

},"index.js":function(require,exports,module){

//////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                          //
// imports/startup/server/index.js                                                                          //
//                                                                                                          //
//////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                            //
let Meteor;
module.watch(require("meteor/meteor"), {
  Meteor(v) {
    Meteor = v;
  }

}, 0);
let loadServerConfig;
module.watch(require("./config"), {
  loadServerConfig(v) {
    loadServerConfig = v;
  }

}, 1);
Meteor.startup(() => {
  let env = process.env.NODE_ENV;
  loadServerConfig(env);
  console.log("ALL OK!");
});
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

}}}},"common":{"routes":{"routes.js":function(require,exports,module){

//////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                          //
// common/routes/routes.js                                                                                  //
//                                                                                                          //
//////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                            //
let FlowRouter;
module.watch(require("meteor/kadira:flow-router"), {
  FlowRouter(v) {
    FlowRouter = v;
  }

}, 0);
let Meteor;
module.watch(require("meteor/meteor"), {
  Meteor(v) {
    Meteor = v;
  }

}, 1);
// Set up all routes in the app
FlowRouter.route('/', {
  name: 'home',

  action() {
    BlazeLayout.render('layoutDefault', {
      content: 'home'
    });
  }

});
FlowRouter.route('/user-details', {
  name: 'userDetails',

  action() {
    BlazeLayout.render('layoutDefault', {
      content: 'userDetails'
    });
  },

  triggersEnter() {
    if (!Meteor.userId()) {
      FlowRouter.go('home');
    }
  }

});
FlowRouter.route('/transferto/:userId', {
  name: 'transferTo',

  action() {
    BlazeLayout.render('layoutDefault', {
      content: 'transferTo'
    });
  }

});
FlowRouter.route('/transfer-result/:transferId', {
  name: 'transferResult',

  action() {
    BlazeLayout.render('layoutDefault', {
      content: 'transferResult'
    });
  }

});
FlowRouter.route('/transfers', {
  name: 'transfers',

  action() {
    BlazeLayout.render('layoutDefault', {
      content: 'transfers'
    });
  }

});
FlowRouter.notFound = {
  action() {
    BlazeLayout.render('layoutDefault', {
      content: 'App_notFound'
    });
  }

}; // Set up all routes in the app.

FlowRouter.route('/login', {
  name: 'login',

  action() {
    BlazeLayout.render('layoutDefault', {
      content: 'login'
    });
  }

}); // Set up all routes in the app.

FlowRouter.route('/register', {
  name: 'register',

  action() {
    BlazeLayout.render('layoutDefault', {
      content: 'register'
    });
  }

});
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

}}},"server":{"main.js":function(require,exports,module){

//////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                          //
// server/main.js                                                                                           //
//                                                                                                          //
//////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                            //
module.watch(require("/imports/startup/server"));
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

}}},{
  "extensions": [
    ".js",
    ".json"
  ]
});
require("/api/eth/common/collections/collections.js");
require("/api/eth/server/methods/ethaccounts.js");
require("/api/eth/server/models/Web3jsWrapper.js");
require("/api/eth/server/publications/ethaccounts.js");
require("/api/eth/server/publications/ethtransfers.js");
require("/api/users/server/publications/users.js");
require("/common/routes/routes.js");
require("/server/main.js");
//# sourceURL=meteor://💻app/app/app.js
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm1ldGVvcjovL/CfkrthcHAvYXBpL2V0aC9jb21tb24vY29sbGVjdGlvbnMvY29sbGVjdGlvbnMuanMiLCJtZXRlb3I6Ly/wn5K7YXBwL2FwaS9ldGgvc2VydmVyL21ldGhvZHMvZXRoYWNjb3VudHMuanMiLCJtZXRlb3I6Ly/wn5K7YXBwL2FwaS9ldGgvc2VydmVyL21vZGVscy9XZWIzanNXcmFwcGVyLmpzIiwibWV0ZW9yOi8v8J+Su2FwcC9hcGkvZXRoL3NlcnZlci9wdWJsaWNhdGlvbnMvZXRoYWNjb3VudHMuanMiLCJtZXRlb3I6Ly/wn5K7YXBwL2FwaS9ldGgvc2VydmVyL3B1YmxpY2F0aW9ucy9ldGh0cmFuc2ZlcnMuanMiLCJtZXRlb3I6Ly/wn5K7YXBwL2FwaS91c2Vycy9zZXJ2ZXIvcHVibGljYXRpb25zL3VzZXJzLmpzIiwibWV0ZW9yOi8v8J+Su2FwcC9pbXBvcnRzL3N0YXJ0dXAvY29tbW9uL2NvbmZpZy5qcyIsIm1ldGVvcjovL/CfkrthcHAvaW1wb3J0cy9zdGFydHVwL3NlcnZlci9jb25maWcuanMiLCJtZXRlb3I6Ly/wn5K7YXBwL2ltcG9ydHMvc3RhcnR1cC9zZXJ2ZXIvaW5kZXguanMiLCJtZXRlb3I6Ly/wn5K7YXBwL2NvbW1vbi9yb3V0ZXMvcm91dGVzLmpzIiwibWV0ZW9yOi8v8J+Su2FwcC9zZXJ2ZXIvbWFpbi5qcyJdLCJuYW1lcyI6WyJtb2R1bGUiLCJleHBvcnQiLCJFdGhhY2NvdW50cyIsIkV0aHRyYW5zZmVycyIsIk1vbmdvIiwid2F0Y2giLCJyZXF1aXJlIiwidiIsIkNvbGxlY3Rpb24iLCJNZXRlb3IiLCJXZWIzanNXcmFwcGVyIiwiTWF0Y2giLCJtZXRob2RzIiwiY3JlYXRlQWNjb3VudCIsImVtYWlsIiwiY2hlY2siLCJTdHJpbmciLCJ1c2VyIiwidXNlcnMiLCJmaW5kT25lIiwiRXJyb3IiLCJfaWQiLCJ1c2VySWQiLCJlcnJvciIsIm1lc3NhZ2UiLCJ3ZWIzIiwiYWNjb3VudCIsImFzc29jaWF0ZUFjY291bnQiLCJyZXN1bHQiLCJnZXRVc2VyQmFsYW5jZSIsImdldEFjY291bnRCYWxhbmNlIiwiYWRkcmVzcyIsImxvYWRBY2NvdW50RnJvbUtleXN0b3JlIiwia2V5U3RvcmVGaWxlIiwia2V5U3RvcmVQYXNzd29yZCIsImdldFdlYjNJbnN0YW5jZSIsImV0aCIsImFjY291bnRzIiwiZGVjcnlwdCIsImxvYWRBY2NvdW50RnJvbVByaXZhdGVLZXkiLCJwcml2YXRlS2V5IiwicHJpdmF0ZUtleVRvQWNjb3VudCIsInRyYW5zZmVyIiwiZnJvbVVzZXIiLCJ0b1VzZXIiLCJhbW91bnQiLCJPbmVPZiIsIk51bWJlciIsImZyb21BY2NvdW50IiwidG9BY2NvdW50IiwidHJhbnNmZXJGcm9tIiwicGFyc2VJbnQiLCJ0aGVuIiwicmVjZWlwdCIsInN0YXR1cyIsImZyb20iLCJ0byIsImdhc1VzZWQiLCJ0cmFuc2FjdGlvbkhhc2giLCJ0cmFuc2ZlcklkIiwiaW5zZXJ0IiwiY2F0Y2giLCJXZWIzIiwiZGVmYXVsdCIsImNvbnN0cnVjdG9yIiwiaHR0cFByb3ZpZGVyIiwic2V0dGluZ3MiLCJORVRXT1JLX1VSTCIsInByb3ZpZGVycyIsIkh0dHBQcm92aWRlciIsImNoYWluSWQiLCJORVRXT1JLX0NIQUlOSUQiLCJ0b2tlbkFkZHJlc3MiLCJUT0tFTl9BRERSRVNTIiwiYWJpT2ZDb250cmFjdCIsIlRPS0VOX0FCSSIsIkpTT04iLCJwYXJzZSIsImNvbnRyYWN0IiwiQ29udHJhY3QiLCJjcmVhdGUiLCJhY2NvdW50RGF0YSIsImV4aXN0aW5nIiwiYWNjb3VudElkIiwiUHJvbWlzZSIsInJlc29sdmUiLCJyZWplY3QiLCJiYWxhbmNlT2YiLCJjYWxsIiwiZGF0YSIsImdhc0xpbWl0IiwiZ2FzUHJpY2UiLCJlc3RpbWF0ZUdhcyIsImVuY29kZUFCSSIsImVzdGltYXRlZEdhcyIsImdldEdhc1ByaWNlIiwicHJpY2UiLCJnZXRUcmFuc2FjdGlvbkNvdW50IiwiY291bnQiLCJzaWduVHJhbnNhY3Rpb24iLCJ2YWx1ZSIsInV0aWxzIiwidG9IZXgiLCJub25jZSIsInRvU3RyaW5nIiwidHJhbnNhY3Rpb24iLCJzZW5kU2lnbmVkVHJhbnNhY3Rpb24iLCJyYXdUcmFuc2FjdGlvbiIsIm9uIiwicHVibGlzaCIsInF1ZXJ5IiwiZmluZCIsImZpZWxkcyIsInVzZXJuYW1lIiwicHJvZmlsZSIsImxvYWRDb25maWciLCJBUFBfTkFNRSIsIlRPS0VOX05BTUUiLCJNQVNURVJfQUNDT1VOVCIsIk1BU1RFUl9BQ0NPVU5UX0tFWVNUT1JFX0ZJTEUiLCJNQVNURVJfQUNDT1VOVF9QQVNTV09SRF9GSUxFIiwiTUFTVEVSX0FDQ09VTlRfUFJJVkFURUtFWV9GSUxFIiwiVE9LRU5fU1lNQk9MIiwibG9hZFNlcnZlckNvbmZpZyIsImVudiIsInN0YXJ0dXAiLCJwcm9jZXNzIiwiTk9ERV9FTlYiLCJjb25zb2xlIiwibG9nIiwiRmxvd1JvdXRlciIsInJvdXRlIiwibmFtZSIsImFjdGlvbiIsIkJsYXplTGF5b3V0IiwicmVuZGVyIiwiY29udGVudCIsInRyaWdnZXJzRW50ZXIiLCJnbyIsIm5vdEZvdW5kIl0sIm1hcHBpbmdzIjoiOzs7Ozs7OztBQUFBQSxPQUFPQyxNQUFQLENBQWM7QUFBQ0MsZUFBWSxNQUFJQSxXQUFqQjtBQUE2QkMsZ0JBQWEsTUFBSUE7QUFBOUMsQ0FBZDtBQUEyRSxJQUFJQyxLQUFKO0FBQVVKLE9BQU9LLEtBQVAsQ0FBYUMsUUFBUSxjQUFSLENBQWIsRUFBcUM7QUFBQ0YsUUFBTUcsQ0FBTixFQUFRO0FBQUNILFlBQU1HLENBQU47QUFBUTs7QUFBbEIsQ0FBckMsRUFBeUQsQ0FBekQ7QUFFOUUsSUFBSUwsY0FBYyxJQUFJRSxNQUFNSSxVQUFWLENBQXFCLGFBQXJCLENBQWxCO0FBQ0EsSUFBSUwsZUFBZSxJQUFJQyxNQUFNSSxVQUFWLENBQXFCLGNBQXJCLENBQW5CLEM7Ozs7Ozs7Ozs7Ozs7QUNIUCxJQUFJQyxNQUFKO0FBQVdULE9BQU9LLEtBQVAsQ0FBYUMsUUFBUSxlQUFSLENBQWIsRUFBc0M7QUFBQ0csU0FBT0YsQ0FBUCxFQUFTO0FBQUNFLGFBQU9GLENBQVA7QUFBUzs7QUFBcEIsQ0FBdEMsRUFBNEQsQ0FBNUQ7QUFBK0QsSUFBSUcsYUFBSjtBQUFrQlYsT0FBT0ssS0FBUCxDQUFhQyxRQUFRLHlCQUFSLENBQWIsRUFBZ0Q7QUFBQ0ksZ0JBQWNILENBQWQsRUFBZ0I7QUFBQ0csb0JBQWNILENBQWQ7QUFBZ0I7O0FBQWxDLENBQWhELEVBQW9GLENBQXBGO0FBQXVGLElBQUlJLEtBQUo7QUFBVVgsT0FBT0ssS0FBUCxDQUFhQyxRQUFRLGNBQVIsQ0FBYixFQUFxQztBQUFDSyxRQUFNSixDQUFOLEVBQVE7QUFBQ0ksWUFBTUosQ0FBTjtBQUFROztBQUFsQixDQUFyQyxFQUF5RCxDQUF6RDtBQUE0RCxJQUFJTCxXQUFKLEVBQWdCQyxZQUFoQjtBQUE2QkgsT0FBT0ssS0FBUCxDQUFhQyxRQUFRLHNDQUFSLENBQWIsRUFBNkQ7QUFBQ0osY0FBWUssQ0FBWixFQUFjO0FBQUNMLGtCQUFZSyxDQUFaO0FBQWMsR0FBOUI7O0FBQStCSixlQUFhSSxDQUFiLEVBQWU7QUFBQ0osbUJBQWFJLENBQWI7QUFBZTs7QUFBOUQsQ0FBN0QsRUFBNkgsQ0FBN0g7QUFLdFJFLE9BQU9HLE9BQVAsQ0FBZTtBQUNiOzs7QUFHQUMsaUJBQWdCQyxLQUFELElBQVc7QUFDeEJDLFVBQU1ELEtBQU4sRUFBYUUsTUFBYjtBQUVBLFVBQU1DLE9BQU9SLE9BQU9TLEtBQVAsQ0FBYUMsT0FBYixDQUFxQjtBQUFFLHdCQUFrQkw7QUFBcEIsS0FBckIsQ0FBYjs7QUFDQSxRQUFJLENBQUNHLElBQUwsRUFBVztBQUNULFlBQU0sSUFBSUcsS0FBSixDQUFXLGdEQUErQ04sS0FBTSxFQUFoRSxDQUFOO0FBQ0QsS0FGRCxNQUVPLElBQUlHLEtBQUtJLEdBQUwsS0FBYVosT0FBT2EsTUFBUCxFQUFqQixFQUFrQztBQUN2QyxhQUFPO0FBQ0xDLGVBQU8sQ0FERjtBQUVMQyxpQkFBVTtBQUZMLE9BQVA7QUFJRDs7QUFFRCxRQUFJQyxPQUFPLElBQUlmLGFBQUosRUFBWDtBQUNBLFVBQU1nQixVQUFVRCxLQUFLWixhQUFMLENBQW1CSSxLQUFLSSxHQUF4QixDQUFoQjs7QUFFQSxRQUFJSyxRQUFRSCxLQUFaLEVBQW1CO0FBQ2pCLGFBQU87QUFDTEEsZUFBTyxDQURGO0FBRUxDLGlCQUFTRSxRQUFRRjtBQUZaLE9BQVA7QUFJRDs7QUFFRCxXQUFPO0FBQ0xBLGVBQVM7QUFESixLQUFQO0FBR0QsR0E5Qlk7O0FBZ0NiOzs7Ozs7QUFNQUcsb0JBQWtCLENBQUNiLEtBQUQsRUFBUVksT0FBUixLQUFvQjtBQUNwQ1gsVUFBTUQsS0FBTixFQUFhRSxNQUFiO0FBRUEsVUFBTUMsT0FBT1IsT0FBT1MsS0FBUCxDQUFhQyxPQUFiLENBQXFCO0FBQUUsd0JBQWtCTDtBQUFwQixLQUFyQixDQUFiOztBQUNBLFFBQUksQ0FBQ0csSUFBTCxFQUFXO0FBQ1QsYUFBTztBQUNMTSxlQUFPLENBREY7QUFFTEMsaUJBQVUsZ0RBQStDVixLQUFNO0FBRjFELE9BQVA7QUFJRCxLQUxELE1BS08sSUFBSUcsS0FBS0ksR0FBTCxLQUFhWixPQUFPYSxNQUFQLEVBQWpCLEVBQWtDO0FBQ3ZDLGFBQU87QUFDTEMsZUFBTyxDQURGO0FBRUxDLGlCQUFVO0FBRkwsT0FBUDtBQUlEOztBQUVELFFBQUlDLE9BQU8sSUFBSWYsYUFBSixFQUFYO0FBQ0EsVUFBTWtCLFNBQVNILEtBQUtFLGdCQUFMLENBQXNCVixLQUFLSSxHQUEzQixFQUFnQ0ssT0FBaEMsQ0FBZjs7QUFFQSxRQUFJRSxPQUFPTCxLQUFYLEVBQWtCO0FBQ2hCLGFBQU87QUFDTEEsZUFBTyxDQURGO0FBRUxDLGlCQUFTSSxPQUFPSjtBQUZYLE9BQVA7QUFJRDs7QUFFRCxXQUFPO0FBQ0xBLGVBQVM7QUFESixLQUFQO0FBR0QsR0FuRVk7O0FBcUViOzs7Ozs7O0FBT0FLLGtCQUFpQlAsTUFBRCxJQUFZO0FBQzFCUCxVQUFNTyxNQUFOLEVBQWNOLE1BQWQ7QUFFQSxVQUFNVSxVQUFVeEIsWUFBWWlCLE9BQVosQ0FBb0I7QUFBRUc7QUFBRixLQUFwQixDQUFoQjs7QUFDQSxRQUFJLENBQUNJLE9BQUwsRUFBYztBQUNaLFlBQU0sSUFBSU4sS0FBSixDQUFXLGdFQUErREUsTUFBTyxFQUFqRixDQUFOO0FBQ0Q7O0FBRUQsUUFBSUcsT0FBTyxJQUFJZixhQUFKLEVBQVg7O0FBQ0EsUUFBSTtBQUNGLGFBQU9lLEtBQUtLLGlCQUFMLENBQXVCSixRQUFRSyxPQUEvQixDQUFQO0FBQ0QsS0FGRCxDQUVFLE9BQU1SLEtBQU4sRUFBYTtBQUNiLGFBQU87QUFDTEEsZUFBTyxDQURGO0FBRUxDLGlCQUFTRDtBQUZKLE9BQVA7QUFJRDtBQUNGLEdBN0ZZOztBQStGYjs7Ozs7Ozs7QUFRQVMsMkJBQXlCLENBQUNDLFlBQUQsRUFBZUMsZ0JBQWYsS0FBb0M7QUFDM0RuQixVQUFNa0IsWUFBTixFQUFvQmpCLE1BQXBCO0FBQ0FELFVBQU1tQixnQkFBTixFQUF3QmxCLE1BQXhCO0FBRUEsUUFBSVMsT0FBTyxJQUFJZixhQUFKLEVBQVg7O0FBQ0EsUUFBSTtBQUNGLGFBQU9lLEtBQUtVLGVBQUwsR0FBdUJDLEdBQXZCLENBQTJCQyxRQUEzQixDQUFvQ0MsT0FBcEMsQ0FBNENMLFlBQTVDLEVBQTBEQyxnQkFBMUQsQ0FBUDtBQUNELEtBRkQsQ0FFRSxPQUFPWCxLQUFQLEVBQWM7QUFDZCxhQUFPO0FBQ0xBLGVBQU8sQ0FERjtBQUVMQyxpQkFBVSx3Q0FBdUNELE1BQU1DLE9BQVE7QUFGMUQsT0FBUDtBQUlEO0FBQ0YsR0FwSFk7O0FBc0hiOzs7Ozs7O0FBT0FlLDZCQUE0QkMsVUFBRCxJQUFnQjtBQUN6Q3pCLFVBQU15QixVQUFOLEVBQWtCeEIsTUFBbEI7QUFFQSxRQUFJUyxPQUFPLElBQUlmLGFBQUosRUFBWDs7QUFDQSxRQUFJO0FBQ0YsYUFBT2UsS0FBS1UsZUFBTCxHQUF1QkMsR0FBdkIsQ0FBMkJDLFFBQTNCLENBQW9DSSxtQkFBcEMsQ0FBd0RELFVBQXhELENBQVA7QUFDRCxLQUZELENBRUUsT0FBT2pCLEtBQVAsRUFBYztBQUNkLGFBQU87QUFDTEEsZUFBTyxDQURGO0FBRUxDLGlCQUFVLDJDQUEwQ0QsTUFBTUMsT0FBUTtBQUY3RCxPQUFQO0FBSUQ7QUFDRixHQXpJWTs7QUEySWI7Ozs7Ozs7Ozs7QUFVQWtCLFlBQVUsQ0FBQ0MsUUFBRCxFQUFXQyxNQUFYLEVBQW1CQyxNQUFuQixLQUE4QjtBQUN0QzlCLFVBQU00QixRQUFOLEVBQWdCM0IsTUFBaEI7QUFDQUQsVUFBTTZCLE1BQU4sRUFBYzVCLE1BQWQ7QUFDQUQsVUFBTThCLE1BQU4sRUFBY2xDLE1BQU1tQyxLQUFOLENBQVk5QixNQUFaLEVBQW9CK0IsTUFBcEIsQ0FBZDs7QUFFQSxRQUFJSixhQUFhbEMsT0FBT2EsTUFBUCxFQUFqQixFQUFrQztBQUNoQyxhQUFPO0FBQ0xDLGVBQU8sQ0FERjtBQUVMQyxpQkFBVTtBQUZMLE9BQVA7QUFJRDs7QUFFRCxVQUFNd0IsY0FBYzlDLFlBQVlpQixPQUFaLENBQW9CO0FBQUVHLGNBQVFxQjtBQUFWLEtBQXBCLENBQXBCO0FBQ0EsVUFBTU0sWUFBWS9DLFlBQVlpQixPQUFaLENBQW9CO0FBQUVHLGNBQVFzQjtBQUFWLEtBQXBCLENBQWxCOztBQUNBLFFBQUksQ0FBQ0ksV0FBRCxJQUFnQixDQUFDQyxTQUFyQixFQUFnQztBQUM5QixhQUFPO0FBQ0wxQixlQUFPLENBREY7QUFFTEMsaUJBQVUsMERBQXlEbUIsUUFBUyxPQUFNQyxNQUFPO0FBRnBGLE9BQVA7QUFJRDs7QUFFRCxRQUFJbkIsT0FBTyxJQUFJZixhQUFKLEVBQVg7QUFDQSxXQUFPZSxLQUFLeUIsWUFBTCxDQUFrQkYsWUFBWWpCLE9BQTlCLEVBQXVDa0IsVUFBVWxCLE9BQWpELEVBQTBEb0IsU0FBU04sTUFBVCxDQUExRCxFQUE0RUcsWUFBWVIsVUFBeEYsRUFDTlksSUFETSxDQUNEQyxXQUFXO0FBQ2YsWUFBTVgsV0FBVztBQUNmWSxnQkFBUSxTQURPO0FBRWZDLGNBQU1GLFFBQVFFLElBRkM7QUFHZkMsWUFBSVAsVUFBVWxCLE9BSEM7QUFJZjBCLGlCQUFTSixRQUFRSSxPQUpGO0FBS2ZDLHlCQUFpQkwsUUFBUUssZUFMVjtBQU1mYixnQkFBUUEsTUFOTztBQU9mUSxpQkFBU0E7QUFQTSxPQUFqQjtBQVVBLFlBQU1NLGFBQWF4RCxhQUFheUQsTUFBYixDQUFvQmxCLFFBQXBCLENBQW5CO0FBQ0FBLGVBQVNyQixHQUFULEdBQWVzQyxVQUFmO0FBRUEsYUFBT2pCLFFBQVA7QUFDRCxLQWhCTSxFQWlCTm1CLEtBakJNLENBaUJBdEMsU0FBUztBQUNkLFlBQU1tQixXQUFXO0FBQ2ZZLGdCQUFRLE9BRE87QUFFZi9CLGVBQU9BO0FBRlEsT0FBakI7QUFJQSxZQUFNb0MsYUFBYXhELGFBQWF5RCxNQUFiLENBQW9CbEIsUUFBcEIsQ0FBbkI7QUFDQUEsZUFBU3JCLEdBQVQsR0FBZXNDLFVBQWY7QUFFQSwwQkFDS2pCLFFBREw7QUFFRW5CLGVBQU87QUFGVDtBQUlELEtBN0JNLENBQVA7QUE4QkQ7QUF6TVksQ0FBZixFOzs7Ozs7Ozs7Ozs7O0FDTEF2QixPQUFPQyxNQUFQLENBQWM7QUFBQ1MsaUJBQWMsTUFBSUE7QUFBbkIsQ0FBZDtBQUFpRCxJQUFJb0QsSUFBSjtBQUFTOUQsT0FBT0ssS0FBUCxDQUFhQyxRQUFRLE1BQVIsQ0FBYixFQUE2QjtBQUFDeUQsVUFBUXhELENBQVIsRUFBVTtBQUFDdUQsV0FBS3ZELENBQUw7QUFBTzs7QUFBbkIsQ0FBN0IsRUFBa0QsQ0FBbEQ7QUFBcUQsSUFBSUUsTUFBSjtBQUFXVCxPQUFPSyxLQUFQLENBQWFDLFFBQVEsZUFBUixDQUFiLEVBQXNDO0FBQUNHLFNBQU9GLENBQVAsRUFBUztBQUFDRSxhQUFPRixDQUFQO0FBQVM7O0FBQXBCLENBQXRDLEVBQTRELENBQTVEO0FBQStELElBQUlMLFdBQUo7QUFBZ0JGLE9BQU9LLEtBQVAsQ0FBYUMsUUFBUSxzQ0FBUixDQUFiLEVBQTZEO0FBQUNKLGNBQVlLLENBQVosRUFBYztBQUFDTCxrQkFBWUssQ0FBWjtBQUFjOztBQUE5QixDQUE3RCxFQUE2RixDQUE3Rjs7QUFJbE0sTUFBTUcsYUFBTixDQUFvQjtBQUN6Qjs7Ozs7OztBQU9Bc0QsY0FBWUMsZUFBZXhELE9BQU95RCxRQUFQLENBQWdCQyxXQUEzQyxFQUF3RDtBQUN0RCxTQUFLMUMsSUFBTCxHQUFZLElBQUlxQyxJQUFKLENBQVMsSUFBSUEsS0FBS00sU0FBTCxDQUFlQyxZQUFuQixDQUFnQ0osWUFBaEMsQ0FBVCxDQUFaLENBRHNELENBR3REOztBQUNBLFNBQUtLLE9BQUwsR0FBZTdELE9BQU95RCxRQUFQLENBQWdCSyxlQUEvQixDQUpzRCxDQUtyRDs7QUFDRCxTQUFLQyxZQUFMLEdBQW9CL0QsT0FBT3lELFFBQVAsQ0FBZ0JPLGFBQXBDO0FBRUEsUUFBSUMsZ0JBQWdCakUsT0FBT3lELFFBQVAsQ0FBZ0JTLFNBQXBDO0FBQ0FELG9CQUFnQkUsS0FBS0MsS0FBTCxDQUFXSCxhQUFYLENBQWhCO0FBQ0EsU0FBS0ksUUFBTCxHQUFnQixJQUFJLEtBQUtyRCxJQUFMLENBQVVXLEdBQVYsQ0FBYzJDLFFBQWxCLENBQTJCTCxhQUEzQixFQUEwQyxLQUFLRixZQUEvQyxDQUFoQjtBQUNEO0FBRUQ7Ozs7Ozs7QUFLQXJDLG9CQUFrQjtBQUNoQixXQUFPLEtBQUtWLElBQVo7QUFDRDtBQUVEOzs7Ozs7Ozs7QUFPQVosZ0JBQWNTLE1BQWQsRUFBc0I7QUFDcEIsUUFBSUksT0FBSjs7QUFFQSxRQUFJO0FBQ0ZBLGdCQUFVLEtBQUtELElBQUwsQ0FBVVcsR0FBVixDQUFjQyxRQUFkLENBQXVCMkMsTUFBdkIsRUFBVjtBQUNELEtBRkQsQ0FFRSxPQUFPekQsS0FBUCxFQUFjO0FBQ2QsYUFBTztBQUNMQSxlQUFPLENBREY7QUFFTEMsaUJBQVUsb0NBQW1DRCxNQUFNQyxPQUFRO0FBRnRELE9BQVA7QUFJRDs7QUFFRCxXQUFPLEtBQUtHLGdCQUFMLENBQXNCTCxNQUF0QixFQUE4QkksT0FBOUIsQ0FBUDtBQUNEO0FBRUQ7Ozs7Ozs7OztBQU9BQyxtQkFBaUJMLE1BQWpCLEVBQXlCSSxPQUF6QixFQUFrQztBQUNoQyxRQUFJdUQsY0FBYztBQUNoQmxELGVBQVNMLFFBQVFLLE9BREQ7QUFFaEJTLGtCQUFZZCxRQUFRYyxVQUZKO0FBR2hCbEIsY0FBUUE7QUFIUSxLQUFsQjtBQU1BLFVBQU00RCxXQUFXaEYsWUFBWWlCLE9BQVosQ0FBb0I7QUFBRVksZUFBU0wsUUFBUUs7QUFBbkIsS0FBcEIsQ0FBakI7O0FBQ0EsUUFBSW1ELFFBQUosRUFBYztBQUNaLGFBQU87QUFDTDNELGVBQU8sQ0FERjtBQUVMQyxpQkFBVTtBQUZMLE9BQVA7QUFJRDs7QUFFRCxRQUFJMkQsWUFBWWpGLFlBQVkwRCxNQUFaLENBQW1CcUIsV0FBbkIsQ0FBaEI7O0FBQ0EsUUFBSSxDQUFDRSxTQUFMLEVBQWdCO0FBQ2QsYUFBTztBQUNMNUQsZUFBTyxDQURGO0FBRUxDLGlCQUFVO0FBRkwsT0FBUDtBQUlEOztBQUVELHdCQUNLeUQsV0FETDtBQUVFNUQsV0FBSzhEO0FBRlA7QUFJRDs7QUFBQ3RCLFFBQU10QyxLQUFOLEVBQWE7QUFDYixXQUFPO0FBQ0xBLGFBQU8sQ0FERjtBQUVMQyxlQUFVLGtDQUFpQ0QsTUFBTUMsT0FBUTtBQUZwRCxLQUFQO0FBSUQ7QUFFRDs7Ozs7Ozs7O0FBT0FNLG9CQUFrQkosT0FBbEIsRUFBMkI7QUFDekIsV0FBTyxJQUFJMEQsT0FBSixDQUFZLENBQUNDLE9BQUQsRUFBVUMsTUFBVixLQUFxQjtBQUN0QyxXQUFLUixRQUFMLENBQWNsRSxPQUFkLENBQ0cyRSxTQURILENBQ2E3RCxPQURiLEVBRUc4RCxJQUZILENBRVE7QUFBQ2pDLGNBQU0sS0FBS2lCO0FBQVosT0FGUixFQUdHcEIsSUFISCxDQUdRcUMsUUFBUTtBQUNaSixnQkFBUUksSUFBUjtBQUNELE9BTEgsRUFNRzVCLEtBTkgsQ0FNU3RDLFNBQVM7QUFDZCtELGVBQU8vRCxLQUFQO0FBQ0E7QUFDRCxPQVRIO0FBVUQsS0FYTSxDQUFQO0FBWUQ7QUFFRDs7Ozs7Ozs7Ozs7O0FBVUEyQixlQUFhRixXQUFiLEVBQTBCQyxTQUExQixFQUFxQ0osTUFBckMsRUFBNkNMLFVBQTdDLEVBQXlEO0FBQ3ZELFFBQUlrRCxRQUFKLEVBQWNDLFFBQWQ7QUFFQSxXQUFPLElBQUlQLE9BQUosQ0FBWSxDQUFDQyxPQUFELEVBQVVDLE1BQVYsS0FBcUI7QUFDdEMsV0FBSzdELElBQUwsQ0FBVVcsR0FBVixDQUFjd0QsV0FBZCxDQUEwQjtBQUN4QnJDLGNBQU1QLFdBRGtCO0FBRXBCUSxZQUFJLEtBQUtnQixZQUZXO0FBR3BCRixpQkFBUyxLQUFLQSxPQUhNO0FBSXBCbUIsY0FBTyxLQUFLWCxRQUFMLENBQWNsRSxPQUFkLENBQXNCOEIsUUFBdEIsQ0FBK0JPLFNBQS9CLEVBQTBDSixNQUExQyxFQUFrRGdELFNBQWxEO0FBSmEsT0FBMUIsRUFNQ3pDLElBTkQsQ0FNTTBDLGdCQUFnQjtBQUNwQkosbUJBQVdJLGVBQWUsQ0FBMUI7QUFFQSxlQUFPLEtBQUtyRSxJQUFMLENBQVVXLEdBQVYsQ0FBYzJELFdBQWQsRUFBUDtBQUNELE9BVkQsRUFXQzNDLElBWEQsQ0FXTTRDLFNBQVM7QUFDYkwsbUJBQVdLLEtBQVg7QUFFQSxlQUFPLEtBQUt2RSxJQUFMLENBQVVXLEdBQVYsQ0FBYzZELG1CQUFkLENBQWtDakQsV0FBbEMsQ0FBUDtBQUNELE9BZkQsRUFnQkNJLElBaEJELENBZ0JNOEMsU0FBUztBQUNiLGVBQU8sS0FBS3pFLElBQUwsQ0FBVVcsR0FBVixDQUFjQyxRQUFkLENBQXVCSSxtQkFBdkIsQ0FBMkNELFVBQTNDLEVBQXVEMkQsZUFBdkQsQ0FBdUU7QUFDNUU1QyxnQkFBTVAsV0FEc0U7QUFFNUVRLGNBQUksS0FBS2dCLFlBRm1FO0FBRzVFNEIsaUJBQU8sS0FIcUU7QUFJNUVULG9CQUFVLEtBQUtsRSxJQUFMLENBQVU0RSxLQUFWLENBQWdCQyxLQUFoQixDQUFzQlgsUUFBdEIsQ0FKa0U7QUFLNUVELG9CQUFVLEtBQUtqRSxJQUFMLENBQVU0RSxLQUFWLENBQWdCQyxLQUFoQixDQUFzQlosUUFBdEIsQ0FMa0U7QUFNNUVhLGlCQUFPLE9BQU9MLE1BQU1NLFFBQU4sQ0FBZSxFQUFmLENBTjhEO0FBTzVFbEMsbUJBQVMsS0FBS0EsT0FQOEQ7QUFRNUVtQixnQkFBTyxLQUFLWCxRQUFMLENBQWNsRSxPQUFkLENBQXNCOEIsUUFBdEIsQ0FBK0JPLFNBQS9CLEVBQTBDSixNQUExQyxFQUFrRGdELFNBQWxEO0FBUnFFLFNBQXZFLENBQVA7QUFVRCxPQTNCRCxFQTRCQ3pDLElBNUJELENBNEJNcUQsZUFBZ0I7QUFDcEIsYUFBS2hGLElBQUwsQ0FBVVcsR0FBVixDQUFjc0UscUJBQWQsQ0FBb0NELFlBQVlFLGNBQWhELEVBQ0dDLEVBREgsQ0FDTSxTQUROLEVBQ2tCdkQsT0FBRCxJQUFhO0FBQzVCO0FBQ0FnQyxrQkFBUWhDLE9BQVI7QUFDRCxTQUpELEVBS0N1RCxFQUxELENBS0ksT0FMSixFQUthckYsU0FBUztBQUNwQjtBQUNBK0QsaUJBQU8vRCxNQUFNQyxPQUFiO0FBQ0QsU0FSRDtBQVNELE9BdENELEVBdUNDcUMsS0F2Q0QsQ0F1Q090QyxTQUFTO0FBQ2QrRCxlQUFPL0QsTUFBTUMsT0FBYjtBQUNELE9BekNEO0FBMENELEtBM0NNLENBQVA7QUE0Q0Q7O0FBNUt3QixDOzs7Ozs7Ozs7OztBQ0ozQixJQUFJZixNQUFKO0FBQVdULE9BQU9LLEtBQVAsQ0FBYUMsUUFBUSxlQUFSLENBQWIsRUFBc0M7QUFBQ0csU0FBT0YsQ0FBUCxFQUFTO0FBQUNFLGFBQU9GLENBQVA7QUFBUzs7QUFBcEIsQ0FBdEMsRUFBNEQsQ0FBNUQ7QUFBK0QsSUFBSUwsV0FBSjtBQUFnQkYsT0FBT0ssS0FBUCxDQUFhQyxRQUFRLHNDQUFSLENBQWIsRUFBNkQ7QUFBQ0osY0FBWUssQ0FBWixFQUFjO0FBQUNMLGtCQUFZSyxDQUFaO0FBQWM7O0FBQTlCLENBQTdELEVBQTZGLENBQTdGO0FBRzFGRSxPQUFPb0csT0FBUCxDQUFlLGFBQWYsRUFBOEIsVUFBU0MsUUFBUSxFQUFqQixFQUFxQjtBQUNqRCxTQUFPNUcsWUFBWTZHLElBQVosQ0FBaUJELEtBQWpCLEVBQXdCO0FBQzdCRSxZQUFRO0FBQUUzRixXQUFLLENBQVA7QUFBVVUsZUFBUyxDQUFuQjtBQUFzQlQsY0FBUTtBQUE5QjtBQURxQixHQUF4QixDQUFQO0FBR0QsQ0FKRCxFOzs7Ozs7Ozs7OztBQ0hBLElBQUliLE1BQUo7QUFBV1QsT0FBT0ssS0FBUCxDQUFhQyxRQUFRLGVBQVIsQ0FBYixFQUFzQztBQUFDRyxTQUFPRixDQUFQLEVBQVM7QUFBQ0UsYUFBT0YsQ0FBUDtBQUFTOztBQUFwQixDQUF0QyxFQUE0RCxDQUE1RDtBQUErRCxJQUFJSixZQUFKO0FBQWlCSCxPQUFPSyxLQUFQLENBQWFDLFFBQVEsc0NBQVIsQ0FBYixFQUE2RDtBQUFDSCxlQUFhSSxDQUFiLEVBQWU7QUFBQ0osbUJBQWFJLENBQWI7QUFBZTs7QUFBaEMsQ0FBN0QsRUFBK0YsQ0FBL0Y7QUFHM0ZFLE9BQU9vRyxPQUFQLENBQWUsY0FBZixFQUErQixVQUFTQyxRQUFRLEVBQWpCLEVBQXFCRSxTQUFTLEVBQTlCLEVBQWtDO0FBQy9ELFNBQU83RyxhQUFhNEcsSUFBYixDQUFrQkQsS0FBbEIsRUFBeUI7QUFDOUJFLFlBQVFBO0FBRHNCLEdBQXpCLENBQVA7QUFHRCxDQUpELEU7Ozs7Ozs7Ozs7O0FDSEEsSUFBSXZHLE1BQUo7QUFBV1QsT0FBT0ssS0FBUCxDQUFhQyxRQUFRLGVBQVIsQ0FBYixFQUFzQztBQUFDRyxTQUFPRixDQUFQLEVBQVM7QUFBQ0UsYUFBT0YsQ0FBUDtBQUFTOztBQUFwQixDQUF0QyxFQUE0RCxDQUE1RDtBQUVYRSxPQUFPb0csT0FBUCxDQUFlLGNBQWYsRUFBK0IsVUFBU0MsUUFBUSxFQUFqQixFQUFxQjtBQUNsRCxTQUFPckcsT0FBT1MsS0FBUCxDQUFhNkYsSUFBYixDQUFrQkQsS0FBbEIsRUFBeUI7QUFDOUJFLFlBQVE7QUFBRUMsZ0JBQVUsQ0FBWjtBQUFlQyxlQUFTO0FBQXhCO0FBRHNCLEdBQXpCLENBQVA7QUFHRCxDQUpELEU7Ozs7Ozs7Ozs7O0FDRkFsSCxPQUFPQyxNQUFQLENBQWM7QUFBQ2tILGNBQVcsTUFBSUE7QUFBaEIsQ0FBZDtBQUEyQyxJQUFJMUcsTUFBSjtBQUFXVCxPQUFPSyxLQUFQLENBQWFDLFFBQVEsZUFBUixDQUFiLEVBQXNDO0FBQUNHLFNBQU9GLENBQVAsRUFBUztBQUFDRSxhQUFPRixDQUFQO0FBQVM7O0FBQXBCLENBQXRDLEVBQTRELENBQTVEOztBQUUvQyxTQUFTNEcsVUFBVCxHQUFzQjtBQUMzQjFHLFNBQU95RCxRQUFQLENBQWdCa0QsUUFBaEIsR0FBMkIsYUFBM0I7QUFDQTNHLFNBQU95RCxRQUFQLENBQWdCTyxhQUFoQixHQUFnQyw0Q0FBaEM7QUFDQWhFLFNBQU95RCxRQUFQLENBQWdCbUQsVUFBaEIsR0FBNkIsYUFBN0I7QUFDQTVHLFNBQU95RCxRQUFQLENBQWdCUyxTQUFoQixHQUE2QixzOUlBQTdCO0FBQ0FsRSxTQUFPeUQsUUFBUCxDQUFnQm9ELGNBQWhCLEdBQWlDLDRDQUFqQyxDQUwyQixDQU0zQjtBQUNBOztBQUNBN0csU0FBT3lELFFBQVAsQ0FBZ0JxRCw0QkFBaEIsR0FBK0MsK0VBQS9DO0FBQ0E5RyxTQUFPeUQsUUFBUCxDQUFnQnNELDRCQUFoQixHQUErQyx3RkFBL0MsQ0FUMkIsQ0FVM0I7QUFDQTs7QUFDQS9HLFNBQU95RCxRQUFQLENBQWdCdUQsOEJBQWhCLEdBQWlELG1DQUFqRDtBQUNBaEgsU0FBT3lELFFBQVAsQ0FBZ0J3RCxZQUFoQixHQUErQixLQUEvQjtBQUNBakgsU0FBT3lELFFBQVAsQ0FBZ0JDLFdBQWhCLEdBQThCLDJCQUE5QixDQWQyQixDQWUzQjs7QUFDQTFELFNBQU95RCxRQUFQLENBQWdCSyxlQUFoQixHQUFrQyxDQUFsQztBQUNELEM7Ozs7Ozs7Ozs7O0FDbkJEdkUsT0FBT0MsTUFBUCxDQUFjO0FBQUMwSCxvQkFBaUIsTUFBSUE7QUFBdEIsQ0FBZDtBQUF1RCxJQUFJUixVQUFKO0FBQWVuSCxPQUFPSyxLQUFQLENBQWFDLFFBQVEsa0JBQVIsQ0FBYixFQUF5QztBQUFDNkcsYUFBVzVHLENBQVgsRUFBYTtBQUFDNEcsaUJBQVc1RyxDQUFYO0FBQWE7O0FBQTVCLENBQXpDLEVBQXVFLENBQXZFOztBQUUvRCxTQUFTb0gsZ0JBQVQsQ0FBMEJDLEdBQTFCLEVBQStCO0FBQ3BDLFVBQVFBLEdBQVI7QUFDRSxTQUFLLFlBQUw7QUFDQSxTQUFLLFNBQUw7QUFDQSxTQUFLLGFBQUw7QUFDQTtBQUNFVDtBQUNBO0FBTko7QUFRRCxDOzs7Ozs7Ozs7OztBQ1hELElBQUkxRyxNQUFKO0FBQVdULE9BQU9LLEtBQVAsQ0FBYUMsUUFBUSxlQUFSLENBQWIsRUFBc0M7QUFBQ0csU0FBT0YsQ0FBUCxFQUFTO0FBQUNFLGFBQU9GLENBQVA7QUFBUzs7QUFBcEIsQ0FBdEMsRUFBNEQsQ0FBNUQ7QUFBK0QsSUFBSW9ILGdCQUFKO0FBQXFCM0gsT0FBT0ssS0FBUCxDQUFhQyxRQUFRLFVBQVIsQ0FBYixFQUFpQztBQUFDcUgsbUJBQWlCcEgsQ0FBakIsRUFBbUI7QUFBQ29ILHVCQUFpQnBILENBQWpCO0FBQW1COztBQUF4QyxDQUFqQyxFQUEyRSxDQUEzRTtBQUcvRkUsT0FBT29ILE9BQVAsQ0FBZSxNQUFNO0FBQ25CLE1BQUlELE1BQU1FLFFBQVFGLEdBQVIsQ0FBWUcsUUFBdEI7QUFDQUosbUJBQWlCQyxHQUFqQjtBQUNBSSxVQUFRQyxHQUFSLENBQVksU0FBWjtBQUNELENBSkQsRTs7Ozs7Ozs7Ozs7QUNIQSxJQUFJQyxVQUFKO0FBQWVsSSxPQUFPSyxLQUFQLENBQWFDLFFBQVEsMkJBQVIsQ0FBYixFQUFrRDtBQUFDNEgsYUFBVzNILENBQVgsRUFBYTtBQUFDMkgsaUJBQVczSCxDQUFYO0FBQWE7O0FBQTVCLENBQWxELEVBQWdGLENBQWhGO0FBQW1GLElBQUlFLE1BQUo7QUFBV1QsT0FBT0ssS0FBUCxDQUFhQyxRQUFRLGVBQVIsQ0FBYixFQUFzQztBQUFDRyxTQUFPRixDQUFQLEVBQVM7QUFBQ0UsYUFBT0YsQ0FBUDtBQUFTOztBQUFwQixDQUF0QyxFQUE0RCxDQUE1RDtBQUc3RztBQUNBMkgsV0FBV0MsS0FBWCxDQUFpQixHQUFqQixFQUFzQjtBQUNwQkMsUUFBTSxNQURjOztBQUVwQkMsV0FBUztBQUNQQyxnQkFBWUMsTUFBWixDQUFtQixlQUFuQixFQUFvQztBQUFFQyxlQUFTO0FBQVgsS0FBcEM7QUFDRDs7QUFKbUIsQ0FBdEI7QUFPQU4sV0FBV0MsS0FBWCxDQUFpQixlQUFqQixFQUFrQztBQUNoQ0MsUUFBTSxhQUQwQjs7QUFFaENDLFdBQVM7QUFDUEMsZ0JBQVlDLE1BQVosQ0FBbUIsZUFBbkIsRUFBb0M7QUFBRUMsZUFBUztBQUFYLEtBQXBDO0FBQ0QsR0FKK0I7O0FBS2hDQyxrQkFBZ0I7QUFDZCxRQUFJLENBQUNoSSxPQUFPYSxNQUFQLEVBQUwsRUFBc0I7QUFDcEI0RyxpQkFBV1EsRUFBWCxDQUFjLE1BQWQ7QUFDRDtBQUNGOztBQVQrQixDQUFsQztBQVlBUixXQUFXQyxLQUFYLENBQWlCLHFCQUFqQixFQUF3QztBQUN0Q0MsUUFBTSxZQURnQzs7QUFFdENDLFdBQVM7QUFDUEMsZ0JBQVlDLE1BQVosQ0FBbUIsZUFBbkIsRUFBb0M7QUFBRUMsZUFBUztBQUFYLEtBQXBDO0FBQ0Q7O0FBSnFDLENBQXhDO0FBT0FOLFdBQVdDLEtBQVgsQ0FBaUIsOEJBQWpCLEVBQWlEO0FBQy9DQyxRQUFNLGdCQUR5Qzs7QUFFL0NDLFdBQVM7QUFDUEMsZ0JBQVlDLE1BQVosQ0FBbUIsZUFBbkIsRUFBb0M7QUFBRUMsZUFBUztBQUFYLEtBQXBDO0FBQ0Q7O0FBSjhDLENBQWpEO0FBT0FOLFdBQVdDLEtBQVgsQ0FBaUIsWUFBakIsRUFBK0I7QUFDN0JDLFFBQU0sV0FEdUI7O0FBRTdCQyxXQUFTO0FBQ1BDLGdCQUFZQyxNQUFaLENBQW1CLGVBQW5CLEVBQW9DO0FBQUVDLGVBQVM7QUFBWCxLQUFwQztBQUNEOztBQUo0QixDQUEvQjtBQU9BTixXQUFXUyxRQUFYLEdBQXNCO0FBQ3BCTixXQUFTO0FBQ1BDLGdCQUFZQyxNQUFaLENBQW1CLGVBQW5CLEVBQW9DO0FBQUVDLGVBQVM7QUFBWCxLQUFwQztBQUNEOztBQUhtQixDQUF0QixDLENBTUE7O0FBQ0FOLFdBQVdDLEtBQVgsQ0FBaUIsUUFBakIsRUFBMkI7QUFDekJDLFFBQU0sT0FEbUI7O0FBRXpCQyxXQUFTO0FBQ1BDLGdCQUFZQyxNQUFaLENBQW1CLGVBQW5CLEVBQW9DO0FBQUVDLGVBQVM7QUFBWCxLQUFwQztBQUNEOztBQUp3QixDQUEzQixFLENBT0E7O0FBQ0FOLFdBQVdDLEtBQVgsQ0FBaUIsV0FBakIsRUFBOEI7QUFDNUJDLFFBQU0sVUFEc0I7O0FBRTVCQyxXQUFTO0FBQ1BDLGdCQUFZQyxNQUFaLENBQW1CLGVBQW5CLEVBQW9DO0FBQUVDLGVBQVM7QUFBWCxLQUFwQztBQUNEOztBQUoyQixDQUE5QixFOzs7Ozs7Ozs7OztBQzNEQXhJLE9BQU9LLEtBQVAsQ0FBYUMsUUFBUSx5QkFBUixDQUFiLEUiLCJmaWxlIjoiL2FwcC5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IE1vbmdvIH0gZnJvbSBcIm1ldGVvci9tb25nb1wiO1xuXG5leHBvcnQgbGV0IEV0aGFjY291bnRzID0gbmV3IE1vbmdvLkNvbGxlY3Rpb24oXCJldGhhY2NvdW50c1wiKTtcbmV4cG9ydCBsZXQgRXRodHJhbnNmZXJzID0gbmV3IE1vbmdvLkNvbGxlY3Rpb24oXCJldGh0cmFuc2ZlcnNcIik7IiwiaW1wb3J0IHsgTWV0ZW9yIH0gZnJvbSBcIm1ldGVvci9tZXRlb3JcIjtcbmltcG9ydCB7IFdlYjNqc1dyYXBwZXIgfSBmcm9tIFwiLi4vbW9kZWxzL1dlYjNqc1dyYXBwZXJcIjtcbmltcG9ydCB7IE1hdGNoIH0gZnJvbSAnbWV0ZW9yL2NoZWNrJ1xuaW1wb3J0IHsgRXRoYWNjb3VudHMsIEV0aHRyYW5zZmVycyB9IGZyb20gXCIuLi8uLi9jb21tb24vY29sbGVjdGlvbnMvY29sbGVjdGlvbnNcIjtcblxuTWV0ZW9yLm1ldGhvZHMoe1xuICAvKipcbiAgICogQ3JlYXRlIGFuIGFjY291bnQgaW4gdGhlIEV0aGVyZXVtIG5ldG93cmsgZm9yIGFuIHVzZXIuXG4gICAqL1xuICBjcmVhdGVBY2NvdW50OiAoZW1haWwpID0+IHtcbiAgICBjaGVjayhlbWFpbCwgU3RyaW5nKTtcblxuICAgIGNvbnN0IHVzZXIgPSBNZXRlb3IudXNlcnMuZmluZE9uZSh7IFwiZW1haWxzLmFkZHJlc3NcIjogZW1haWwgfSk7XG4gICAgaWYgKCF1c2VyKSB7XG4gICAgICB0aHJvdyBuZXcgRXJyb3IoYEVSUk9SIGNyZWF0aW5nIEV0aCBhY2NvdW50IC0gVXNlciBub3QgZm91bmQ6ICR7ZW1haWx9YCk7XG4gICAgfSBlbHNlIGlmICh1c2VyLl9pZCAhPT0gTWV0ZW9yLnVzZXJJZCgpKSB7XG4gICAgICByZXR1cm4ge1xuICAgICAgICBlcnJvcjogMSxcbiAgICAgICAgbWVzc2FnZTogYEVSUk9SIGNyZWF0aW5nIEV0aCBhY2NvdW50IC0gWW91IGFyZSBub3QgYWxsb3dlZCB0byB1c2UgdGhpcyB1c2VyLmBcbiAgICAgIH1cbiAgICB9XG5cbiAgICBsZXQgd2ViMyA9IG5ldyBXZWIzanNXcmFwcGVyKCk7XG4gICAgY29uc3QgYWNjb3VudCA9IHdlYjMuY3JlYXRlQWNjb3VudCh1c2VyLl9pZCk7XG5cbiAgICBpZiAoYWNjb3VudC5lcnJvcikge1xuICAgICAgcmV0dXJuIHtcbiAgICAgICAgZXJyb3I6IDEsXG4gICAgICAgIG1lc3NhZ2U6IGFjY291bnQubWVzc2FnZSxcbiAgICAgIH1cbiAgICB9XG4gICBcbiAgICByZXR1cm4ge1xuICAgICAgbWVzc2FnZTogXCJUaGUgYWNjb3VudCB3YXMgY3JlYXRlZCBzdWNjZXNzZnVsbHkgaW4gdGhlIEV0aGVyZXVtIG5ldHdvcmshLlwiLFxuICAgIH1cbiAgfSxcblxuICAvKipcbiAgICogQXNzb2NpYXRlIGFuIHVzZXIgdG8gYW4gZXhpc3RpbmcgRXRoZXJldW0gYWNjb3VudC5cbiAgICogXG4gICAqIEBwYXJhbSB7U3RyaW5nfSBlbWFpbCAtIFVzZXIncyBlbWFpbC4gSXQgbXVzdCBhbHJlYWR5IGV4aXN0IGluIHRoZSBwbGF0Zm9ybS5cbiAgICogQHBhcmFtIHtTdHJpbmd9IGFjY291bnQgLSBFdGhlcmV1bSBhY2NvdW50LlxuICAgKi9cbiAgYXNzb2NpYXRlQWNjb3VudDogKGVtYWlsLCBhY2NvdW50KSA9PiB7XG4gICAgY2hlY2soZW1haWwsIFN0cmluZyk7XG5cbiAgICBjb25zdCB1c2VyID0gTWV0ZW9yLnVzZXJzLmZpbmRPbmUoeyBcImVtYWlscy5hZGRyZXNzXCI6IGVtYWlsIH0pO1xuICAgIGlmICghdXNlcikge1xuICAgICAgcmV0dXJuIHtcbiAgICAgICAgZXJyb3I6IDEsXG4gICAgICAgIG1lc3NhZ2U6IGBFUlJPUiBjcmVhdGluZyBFdGggYWNjb3VudCAtIFVzZXIgbm90IGZvdW5kOiAke2VtYWlsfWBcbiAgICAgIH1cbiAgICB9IGVsc2UgaWYgKHVzZXIuX2lkICE9PSBNZXRlb3IudXNlcklkKCkpIHtcbiAgICAgIHJldHVybiB7XG4gICAgICAgIGVycm9yOiAxLFxuICAgICAgICBtZXNzYWdlOiBgRVJST1IgY3JlYXRpbmcgRXRoIGFjY291bnQgLSBZb3UgYXJlIG5vdCBhbGxvd2VkIHRvIHVzZSB0aGlzIHVzZXIuYFxuICAgICAgfVxuICAgIH1cblxuICAgIGxldCB3ZWIzID0gbmV3IFdlYjNqc1dyYXBwZXIoKTtcbiAgICBjb25zdCByZXN1bHQgPSB3ZWIzLmFzc29jaWF0ZUFjY291bnQodXNlci5faWQsIGFjY291bnQpO1xuXG4gICAgaWYgKHJlc3VsdC5lcnJvcikge1xuICAgICAgcmV0dXJuIHtcbiAgICAgICAgZXJyb3I6IDEsXG4gICAgICAgIG1lc3NhZ2U6IHJlc3VsdC5tZXNzYWdlLFxuICAgICAgfVxuICAgIH1cbiAgIFxuICAgIHJldHVybiB7XG4gICAgICBtZXNzYWdlOiBcIlRoZSBhY2NvdW50IHdhcyBjcmVhdGVkIHN1Y2Nlc3NmdWxseSBhc3NvY2lhdGVkIS5cIixcbiAgICB9XG4gIH0sXG5cbiAgLyoqXG4gICAqIEdldCB0aGUgYmFsYW5jZSBvZiBhbiB1c2VyLlxuICAgKlxuICAgKiBAcGFyYW0ge1N0cmluZ30gdXNlcklkIC0gVGhlIHVzZXIgaWQuXG4gICAqIFxuICAgKiBAcmV0dXJucyB7TnVtYmVyfSAtIFRoZSBhY2NvdW50IGJhbGFuY2UuXG4gICAqL1xuICBnZXRVc2VyQmFsYW5jZTogKHVzZXJJZCkgPT4ge1xuICAgIGNoZWNrKHVzZXJJZCwgU3RyaW5nKTtcblxuICAgIGNvbnN0IGFjY291bnQgPSBFdGhhY2NvdW50cy5maW5kT25lKHsgdXNlcklkIH0pO1xuICAgIGlmICghYWNjb3VudCkge1xuICAgICAgdGhyb3cgbmV3IEVycm9yKGBFUlJPUiByZXRyaWV2aW5nIHVzZXIgYmFsYW5jZTogTm90IGZvdW5kIGFjY291bnQgZm9yIHVzZXJJZDogJHt1c2VySWR9YCk7XG4gICAgfVxuXG4gICAgbGV0IHdlYjMgPSBuZXcgV2ViM2pzV3JhcHBlcigpO1xuICAgIHRyeSB7XG4gICAgICByZXR1cm4gd2ViMy5nZXRBY2NvdW50QmFsYW5jZShhY2NvdW50LmFkZHJlc3MpO1xuICAgIH0gY2F0Y2goZXJyb3IpIHtcbiAgICAgIHJldHVybiB7XG4gICAgICAgIGVycm9yOiAxLFxuICAgICAgICBtZXNzYWdlOiBlcnJvclxuICAgICAgfTtcbiAgICB9XG4gIH0sXG5cbiAgLyoqXG4gICAqIERlY3J5cHRzIGEga2V5c3RvcmUgdjMgSlNPTiByZXR1cm5pbmcgaXRzIGFjY291bnQuXG4gICAqXG4gICAqIEBwYXJhbSB7Kn0ga2V5U3RvcmVGaWxlIC0gVGhlIGVuY3J5cHRlZCBwcml2YXRlIGtleSB0byBkZWNyeXB0LlxuICAgKiBAcGFyYW0geyp9IGtleVN0b3JlUGFzc3dvcmQgLSBUaGUgcGFzc3dvcmQgdXNlZCBmb3IgZW5jcnlwdGlvbi5cbiAgICogXG4gICAqIEByZXR1cm5zIHtPYmplY3R9ID0gVGhlIEV0aGVyZXVtIGFjY291bnQuXG4gICAqL1xuICBsb2FkQWNjb3VudEZyb21LZXlzdG9yZTogKGtleVN0b3JlRmlsZSwga2V5U3RvcmVQYXNzd29yZCkgPT4ge1xuICAgIGNoZWNrKGtleVN0b3JlRmlsZSwgU3RyaW5nKTtcbiAgICBjaGVjayhrZXlTdG9yZVBhc3N3b3JkLCBTdHJpbmcpO1xuXG4gICAgbGV0IHdlYjMgPSBuZXcgV2ViM2pzV3JhcHBlcigpO1xuICAgIHRyeSB7XG4gICAgICByZXR1cm4gd2ViMy5nZXRXZWIzSW5zdGFuY2UoKS5ldGguYWNjb3VudHMuZGVjcnlwdChrZXlTdG9yZUZpbGUsIGtleVN0b3JlUGFzc3dvcmQpO1xuICAgIH0gY2F0Y2ggKGVycm9yKSB7XG4gICAgICByZXR1cm4ge1xuICAgICAgICBlcnJvcjogMSxcbiAgICAgICAgbWVzc2FnZTogYEVSUk9SIGxvYWRpbmcgYWNjb3VudCBmcm9tIEtleXN0b3JlOiAke2Vycm9yLm1lc3NhZ2V9YFxuICAgICAgfVxuICAgIH1cbiAgfSxcblxuICAvKipcbiAgICogQ3JlYXRlcyBhbiBhY2NvdW50IG9iamVjdCBmcm9tIGEgcHJpdmF0ZSBrZXkuXG4gICAqXG4gICAqIEBwYXJhbSB7U3RyaW5nfSBwcml2YXRlS2V5IC0gVGhlIGFjY291bnQgcHJpdmF0ZSBrZXkuXG4gICAqIFxuICAgKiBAcmV0dXJucyB7T2JqZWN0fSA9IFRoZSBFdGhlcmV1bSBhY2NvdW50LlxuICAgKi9cbiAgbG9hZEFjY291bnRGcm9tUHJpdmF0ZUtleTogKHByaXZhdGVLZXkpID0+IHtcbiAgICBjaGVjayhwcml2YXRlS2V5LCBTdHJpbmcpO1xuXG4gICAgbGV0IHdlYjMgPSBuZXcgV2ViM2pzV3JhcHBlcigpO1xuICAgIHRyeSB7XG4gICAgICByZXR1cm4gd2ViMy5nZXRXZWIzSW5zdGFuY2UoKS5ldGguYWNjb3VudHMucHJpdmF0ZUtleVRvQWNjb3VudChwcml2YXRlS2V5KTtcbiAgICB9IGNhdGNoIChlcnJvcikge1xuICAgICAgcmV0dXJuIHtcbiAgICAgICAgZXJyb3I6IDEsXG4gICAgICAgIG1lc3NhZ2U6IGBFUlJPUiBsb2FkaW5nIGFjY291bnQgZnJvbSBQcml2YXRlIEtleTogJHtlcnJvci5tZXNzYWdlfWBcbiAgICAgIH1cbiAgICB9XG4gIH0sXG5cbiAgLyoqXG4gICAqIFRyYW5zZmVyIHRva2VucyBiZXR3ZWVuIHVzZXJzLlxuICAgKiBcbiAgICogQHBhcmFtIHtTdHJpbmd9IGZyb21Vc2VyIC0gc2VuZGVyIHVzZXIuIEl0IGlzIHJlcXVpcmVkIHRvIGhhdmVcbiAgICogIHRva2VucyBhbmQgRXRoZXIgaW4gdGhlIGN1cnJlbnQgbmV0d29yay5cbiAgICogQHBhcmFtIHtTdHJpbmd9IHRvVXNlciAtIHJlY2lwaWVudCBhY2NvdW50IHVzZXIuXG4gICAqIEBwYXJhbSB7U3RyaW5nfE51bWJlcn0gYW1vdW50IC0gYW1vdW50IG9mIHRva2VucyB0byBzZW5kLlxuICAgKiBcbiAgICogQHJldHVybnMge1Byb21pc2U8T2JqZWN0Pn0gLSBUaGUgdHJhbnNhY3Rpb24gcmVjZWlwdC5cbiAgICovXG4gIHRyYW5zZmVyOiAoZnJvbVVzZXIsIHRvVXNlciwgYW1vdW50KSA9PiB7XG4gICAgY2hlY2soZnJvbVVzZXIsIFN0cmluZyk7XG4gICAgY2hlY2sodG9Vc2VyLCBTdHJpbmcpO1xuICAgIGNoZWNrKGFtb3VudCwgTWF0Y2guT25lT2YoU3RyaW5nLCBOdW1iZXIpKTtcbiAgICBcbiAgICBpZiAoZnJvbVVzZXIgIT09IE1ldGVvci51c2VySWQoKSkge1xuICAgICAgcmV0dXJuIHtcbiAgICAgICAgZXJyb3I6IDEsXG4gICAgICAgIG1lc3NhZ2U6IGBFUlJPUiB0cmFuc2ZlcmluZyB0b2tlbjogWW91IGFyZSBub3QgYWxsb3dlZCB0byB0cmFuc2ZlciBmcm9tIHRoaXMgdXNlci5gXG4gICAgICB9XG4gICAgfVxuXG4gICAgY29uc3QgZnJvbUFjY291bnQgPSBFdGhhY2NvdW50cy5maW5kT25lKHsgdXNlcklkOiBmcm9tVXNlciB9KTtcbiAgICBjb25zdCB0b0FjY291bnQgPSBFdGhhY2NvdW50cy5maW5kT25lKHsgdXNlcklkOiB0b1VzZXIgfSk7XG4gICAgaWYgKCFmcm9tQWNjb3VudCB8fCAhdG9BY2NvdW50KSB7XG4gICAgICByZXR1cm4ge1xuICAgICAgICBlcnJvcjogMSxcbiAgICAgICAgbWVzc2FnZTogYEVSUk9SIHRyYW5zZmVyaW5nIHRva2VuOiBOb3QgZm91bmQgYWNjb3VudCBmb3IgdXNlcklkOiAke2Zyb21Vc2VyfSBvciAke3RvVXNlcn1gXG4gICAgICB9XG4gICAgfVxuXG4gICAgbGV0IHdlYjMgPSBuZXcgV2ViM2pzV3JhcHBlcigpO1xuICAgIHJldHVybiB3ZWIzLnRyYW5zZmVyRnJvbShmcm9tQWNjb3VudC5hZGRyZXNzLCB0b0FjY291bnQuYWRkcmVzcywgcGFyc2VJbnQoYW1vdW50KSwgZnJvbUFjY291bnQucHJpdmF0ZUtleSlcbiAgICAudGhlbihyZWNlaXB0ID0+IHtcbiAgICAgIGNvbnN0IHRyYW5zZmVyID0ge1xuICAgICAgICBzdGF0dXM6ICdzdWNjZXNzJyxcbiAgICAgICAgZnJvbTogcmVjZWlwdC5mcm9tLFxuICAgICAgICB0bzogdG9BY2NvdW50LmFkZHJlc3MsXG4gICAgICAgIGdhc1VzZWQ6IHJlY2VpcHQuZ2FzVXNlZCxcbiAgICAgICAgdHJhbnNhY3Rpb25IYXNoOiByZWNlaXB0LnRyYW5zYWN0aW9uSGFzaCxcbiAgICAgICAgYW1vdW50OiBhbW91bnQsXG4gICAgICAgIHJlY2VpcHQ6IHJlY2VpcHRcbiAgICAgIH07XG5cbiAgICAgIGNvbnN0IHRyYW5zZmVySWQgPSBFdGh0cmFuc2ZlcnMuaW5zZXJ0KHRyYW5zZmVyKTtcbiAgICAgIHRyYW5zZmVyLl9pZCA9IHRyYW5zZmVySWQ7XG5cbiAgICAgIHJldHVybiB0cmFuc2ZlcjtcbiAgICB9KVxuICAgIC5jYXRjaChlcnJvciA9PiB7XG4gICAgICBjb25zdCB0cmFuc2ZlciA9IHtcbiAgICAgICAgc3RhdHVzOiAnZXJyb3InLFxuICAgICAgICBlcnJvcjogZXJyb3JcbiAgICAgIH07XG4gICAgICBjb25zdCB0cmFuc2ZlcklkID0gRXRodHJhbnNmZXJzLmluc2VydCh0cmFuc2Zlcik7XG4gICAgICB0cmFuc2Zlci5faWQgPSB0cmFuc2ZlcklkO1xuXG4gICAgICByZXR1cm4ge1xuICAgICAgICAuLi50cmFuc2ZlcixcbiAgICAgICAgZXJyb3I6IDEsXG4gICAgICB9O1xuICAgIH0pO1xuICB9XG59KTsiLCJpbXBvcnQgV2ViMyBmcm9tICd3ZWIzJztcbmltcG9ydCB7IE1ldGVvciB9IGZyb20gXCJtZXRlb3IvbWV0ZW9yXCI7XG5pbXBvcnQgeyBFdGhhY2NvdW50cyB9IGZyb20gJy4uLy4uL2NvbW1vbi9jb2xsZWN0aW9ucy9jb2xsZWN0aW9ucyc7XG5cbmV4cG9ydCBjbGFzcyBXZWIzanNXcmFwcGVyIHtcbiAgLyoqXG4gICAqIFdlYjNqc1dyYXBwZXIuXG4gICAqIFxuICAgKiBJbml0aWFsaXNlIFdlYjMgb2JqZWN0IHdpdGggcGxhdGZvcm0tZGVwZW5kYW50IHBhcmFtZXRlcnMuXG4gICAqIFxuICAgKiBAcGFyYW0geyp9IGh0dHBQcm92aWRlciAtIEV0aGVyZXVtIG5ldHdvcmsuXG4gICAqL1xuICBjb25zdHJ1Y3RvcihodHRwUHJvdmlkZXIgPSBNZXRlb3Iuc2V0dGluZ3MuTkVUV09SS19VUkwpIHtcbiAgICB0aGlzLndlYjMgPSBuZXcgV2ViMyhuZXcgV2ViMy5wcm92aWRlcnMuSHR0cFByb3ZpZGVyKGh0dHBQcm92aWRlcikpO1xuICAgIFxuICAgIC8vIENoYWluSWQgaXMgMyBmb3IgdGhlIHRlc3QgbmV0d29yayByb3BzdGVuLiBcbiAgICB0aGlzLmNoYWluSWQgPSBNZXRlb3Iuc2V0dGluZ3MuTkVUV09SS19DSEFJTklEO1xuICAgICAvLyBUaGlzIGlzIHRoZSBjb250cmFjdCBkZXBsb3llZCB0byB0aGUgRXRoIG5ldHdvcmsgd2l0aCB0aGUgdG9rZW4gaW5mby5cbiAgICB0aGlzLnRva2VuQWRkcmVzcyA9IE1ldGVvci5zZXR0aW5ncy5UT0tFTl9BRERSRVNTO1xuICAgIFxuICAgIGxldCBhYmlPZkNvbnRyYWN0ID0gTWV0ZW9yLnNldHRpbmdzLlRPS0VOX0FCSTtcbiAgICBhYmlPZkNvbnRyYWN0ID0gSlNPTi5wYXJzZShhYmlPZkNvbnRyYWN0KTtcbiAgICB0aGlzLmNvbnRyYWN0ID0gbmV3IHRoaXMud2ViMy5ldGguQ29udHJhY3QoYWJpT2ZDb250cmFjdCwgdGhpcy50b2tlbkFkZHJlc3MpO1xuICB9XG5cbiAgLyoqXG4gICAqIEdldCB0aGUgV2ViMyBpbnN0YW5jZS5cbiAgICogXG4gICAqIEByZXR1cm4ge09iamVjdH0gLSBXZWIzIGluc3RhbmNlLiAgXG4gICAqL1xuICBnZXRXZWIzSW5zdGFuY2UoKSB7XG4gICAgcmV0dXJuIHRoaXMud2ViMztcbiAgfVxuXG4gIC8qKlxuICAgKiBDcmVhdGUgYW4gYWNjb3VudCBpbiB0aGUgZXRoZXJldW0gbmV0d29yayBmb3IgYW4gdXNlci5cbiAgICogXG4gICAqIEBwYXJhbSB7U3RyaW5nfSB1c2VySWQgLSBJZCBvZiB0aGUgdXNlci5cbiAgICpcbiAgICogQHJldHVybiB7T2JqZWN0fSAtIENyZWF0ZWQgRXRoZXJldW0gYWNjb3VudC4gIFxuICAgKi9cbiAgY3JlYXRlQWNjb3VudCh1c2VySWQpIHtcbiAgICBsZXQgYWNjb3VudDtcblxuICAgIHRyeSB7XG4gICAgICBhY2NvdW50ID0gdGhpcy53ZWIzLmV0aC5hY2NvdW50cy5jcmVhdGUoKTtcbiAgICB9IGNhdGNoIChlcnJvcikge1xuICAgICAgcmV0dXJuIHtcbiAgICAgICAgZXJyb3I6IDEsXG4gICAgICAgIG1lc3NhZ2U6IGBFUlJPUiBjcmVhdGluZyBFdGhlcmV1bSBhY2NvdW50OiAke2Vycm9yLm1lc3NhZ2V9YFxuICAgICAgfVxuICAgIH1cbiAgICBcbiAgICByZXR1cm4gdGhpcy5hc3NvY2lhdGVBY2NvdW50KHVzZXJJZCwgYWNjb3VudCk7XG4gIH1cblxuICAvKipcbiAgICogQXNzb2NpYXRlIGFuIEV0aGVyZXVtIGFjY291bnQgd2l0aCBhbiB1c2VyLlxuICAgKiBcbiAgICogQHBhcmFtIHtTdHJpbmd9IHVzZXJJZCAtIElkIG9mIHRoZSB1c2VyLlxuICAgKlxuICAgKiBAcmV0dXJuIHtPYmplY3R9IC0gQ3JlYXRlZCBFdGhlcmV1bSBhY2NvdW50LlxuICAgKi9cbiAgYXNzb2NpYXRlQWNjb3VudCh1c2VySWQsIGFjY291bnQpIHtcbiAgICBsZXQgYWNjb3VudERhdGEgPSB7XG4gICAgICBhZGRyZXNzOiBhY2NvdW50LmFkZHJlc3MsXG4gICAgICBwcml2YXRlS2V5OiBhY2NvdW50LnByaXZhdGVLZXksXG4gICAgICB1c2VySWQ6IHVzZXJJZFxuICAgIH07XG5cbiAgICBjb25zdCBleGlzdGluZyA9IEV0aGFjY291bnRzLmZpbmRPbmUoeyBhZGRyZXNzOiBhY2NvdW50LmFkZHJlc3MgfSk7XG4gICAgaWYgKGV4aXN0aW5nKSB7XG4gICAgICByZXR1cm4ge1xuICAgICAgICBlcnJvcjogMSxcbiAgICAgICAgbWVzc2FnZTogYEVSUk9SIGFzc29jaWF0aW5nIEV0aGVyZXVtIGFjY291bnQgdG8gdGhlIHVzZXI6IFRoZSBhY2NvdW50IGFscmVhZHkgZXhpc3RgXG4gICAgICB9XG4gICAgfVxuXG4gICAgbGV0IGFjY291bnRJZCA9IEV0aGFjY291bnRzLmluc2VydChhY2NvdW50RGF0YSk7XG4gICAgaWYgKCFhY2NvdW50SWQpIHtcbiAgICAgIHJldHVybiB7XG4gICAgICAgIGVycm9yOiAxLFxuICAgICAgICBtZXNzYWdlOiBgRVJST1IgYXNzb2NpYXRpbmcgRXRoZXJldW0gYWNjb3VudDogVGhlIGFjY291bnQgY291bGRuJ3QgYmUgcGVyc2lzdGVkIGluIERCYFxuICAgICAgfVxuICAgIH1cblxuICAgIHJldHVybiB7XG4gICAgICAuLi5hY2NvdW50RGF0YSxcbiAgICAgIF9pZDogYWNjb3VudElkXG4gICAgfVxuICB9IGNhdGNoKGVycm9yKSB7XG4gICAgcmV0dXJuIHtcbiAgICAgIGVycm9yOiAxLFxuICAgICAgbWVzc2FnZTogYEVSUk9SIGFzc29jaWF0aW5nIEV0aCBhY2NvdW50OiAke2Vycm9yLm1lc3NhZ2V9YFxuICAgIH1cbiAgfVxuXG4gIC8qKlxuICAgKiBHZXQgdGhlIGJhbGFuY2Ugb2YgYW4gYWNjb3VudC5cbiAgICpcbiAgICogQHBhcmFtIHtTdHJpbmd9IGFjY291bnQgLSBBY2NvdW50IGFkZHJlc3MuXG4gICAqIFxuICAgKiBAcmV0dXJucyB7UHJvbWlzZTxudW1iZXI+fSAtIFRoZSBhY2NvdW50IGJhbGFuY2UuXG4gICAqL1xuICBnZXRBY2NvdW50QmFsYW5jZShhY2NvdW50KSB7XG4gICAgcmV0dXJuIG5ldyBQcm9taXNlKChyZXNvbHZlLCByZWplY3QpID0+IHtcbiAgICAgIHRoaXMuY29udHJhY3QubWV0aG9kc1xuICAgICAgICAuYmFsYW5jZU9mKGFjY291bnQpXG4gICAgICAgIC5jYWxsKHtmcm9tOiB0aGlzLnRva2VuQWRkcmVzc30pXG4gICAgICAgIC50aGVuKGRhdGEgPT4ge1xuICAgICAgICAgIHJlc29sdmUoZGF0YSlcbiAgICAgICAgfSlcbiAgICAgICAgLmNhdGNoKGVycm9yID0+IHtcbiAgICAgICAgICByZWplY3QoZXJyb3IpO1xuICAgICAgICAgIHJldHVybjtcbiAgICAgICAgfSk7XG4gICAgfSk7XG4gIH1cbiAgXG4gIC8qKlxuICAgKiBUcmFuc2ZlciB0b2tlbnMgYmV0d2VlbiBhY2NvdW50cy5cbiAgICogXG4gICAqIEBwYXJhbSB7U3RyaW5nfSBmcm9tQWNjb3VudCAtIHNlbmRlciBhY2NvdW50IGFkZHJlc3MuIEl0IGlzIHJlcXVpcmVkIHRvIGhhdmVcbiAgICogIHRva2VucyBhbmQgRXRoZXIuXG4gICAqIEBwYXJhbSB7U3RyaW5nfSB0b0FjY291bnQgLSByZWNpcGllbnQgYWNjb3VudCBhZGRyZXNzLlxuICAgKiBAcGFyYW0ge1N0cmluZ30gYW1vdW50IC0gdG9rZW5zIHRvIHNlbmQuXG4gICAqIFxuICAgKiBAcmV0dXJucyB7UHJvbWlzZTxPYmplY3Q+fSAtIFRoZSB0cmFuc2FjdGlvbiByZWNlaXB0LlxuICAgKi9cbiAgdHJhbnNmZXJGcm9tKGZyb21BY2NvdW50LCB0b0FjY291bnQsIGFtb3VudCwgcHJpdmF0ZUtleSkge1xuICAgIGxldCBnYXNMaW1pdCwgZ2FzUHJpY2U7XG4gICAgXG4gICAgcmV0dXJuIG5ldyBQcm9taXNlKChyZXNvbHZlLCByZWplY3QpID0+IHtcbiAgICAgIHRoaXMud2ViMy5ldGguZXN0aW1hdGVHYXMoe1xuICAgICAgICBmcm9tOiBmcm9tQWNjb3VudCxcbiAgICAgICAgICAgIHRvOiB0aGlzLnRva2VuQWRkcmVzcyxcbiAgICAgICAgICAgIGNoYWluSWQ6IHRoaXMuY2hhaW5JZCxcbiAgICAgICAgICAgIGRhdGEgOiB0aGlzLmNvbnRyYWN0Lm1ldGhvZHMudHJhbnNmZXIodG9BY2NvdW50LCBhbW91bnQpLmVuY29kZUFCSSgpXG4gICAgICB9KVxuICAgICAgLnRoZW4oZXN0aW1hdGVkR2FzID0+IHtcbiAgICAgICAgZ2FzTGltaXQgPSBlc3RpbWF0ZWRHYXMgKiAyO1xuXG4gICAgICAgIHJldHVybiB0aGlzLndlYjMuZXRoLmdldEdhc1ByaWNlKClcbiAgICAgIH0pXG4gICAgICAudGhlbihwcmljZSA9PiB7XG4gICAgICAgIGdhc1ByaWNlID0gcHJpY2U7XG4gICAgICAgIFxuICAgICAgICByZXR1cm4gdGhpcy53ZWIzLmV0aC5nZXRUcmFuc2FjdGlvbkNvdW50KGZyb21BY2NvdW50KTtcbiAgICAgIH0pXG4gICAgICAudGhlbihjb3VudCA9PiB7XG4gICAgICAgIHJldHVybiB0aGlzLndlYjMuZXRoLmFjY291bnRzLnByaXZhdGVLZXlUb0FjY291bnQocHJpdmF0ZUtleSkuc2lnblRyYW5zYWN0aW9uKHtcbiAgICAgICAgICBmcm9tOiBmcm9tQWNjb3VudCxcbiAgICAgICAgICB0bzogdGhpcy50b2tlbkFkZHJlc3MsXG4gICAgICAgICAgdmFsdWU6ICcweDAnLFxuICAgICAgICAgIGdhc1ByaWNlOiB0aGlzLndlYjMudXRpbHMudG9IZXgoZ2FzUHJpY2UpLFxuICAgICAgICAgIGdhc0xpbWl0OiB0aGlzLndlYjMudXRpbHMudG9IZXgoZ2FzTGltaXQpLFxuICAgICAgICAgIG5vbmNlOiBcIjB4XCIgKyBjb3VudC50b1N0cmluZygxNiksXG4gICAgICAgICAgY2hhaW5JZDogdGhpcy5jaGFpbklkLFxuICAgICAgICAgIGRhdGEgOiB0aGlzLmNvbnRyYWN0Lm1ldGhvZHMudHJhbnNmZXIodG9BY2NvdW50LCBhbW91bnQpLmVuY29kZUFCSSgpXG4gICAgICAgIH0pO1xuICAgICAgfSlcbiAgICAgIC50aGVuKHRyYW5zYWN0aW9uID0+ICB7XG4gICAgICAgIHRoaXMud2ViMy5ldGguc2VuZFNpZ25lZFRyYW5zYWN0aW9uKHRyYW5zYWN0aW9uLnJhd1RyYW5zYWN0aW9uKVxuICAgICAgICAgIC5vbigncmVjZWlwdCcsIChyZWNlaXB0KSA9PiB7XG4gICAgICAgICAgLy8gY29uc29sZS5sb2coJ1RyYW5zZmVyIHRyYW5zYWN0aW9uIGZpbmlzaGVkIHN1Y2Nlc2Z1bGx5OicsIHJlY2VpcHQpO1xuICAgICAgICAgIHJlc29sdmUocmVjZWlwdCk7XG4gICAgICAgIH0pXG4gICAgICAgIC5vbignZXJyb3InLCBlcnJvciA9PiB7XG4gICAgICAgICAgLy8gY29uc29sZS5sb2coJ0VSUk9SIGluIHRyYW5zZmVyIHRyYW5zYWN0aW9uOicsIGVycm9yLm1lc3NhZ2UpO1xuICAgICAgICAgIHJlamVjdChlcnJvci5tZXNzYWdlKTtcbiAgICAgICAgfSk7XG4gICAgICB9KVxuICAgICAgLmNhdGNoKGVycm9yID0+IHtcbiAgICAgICAgcmVqZWN0KGVycm9yLm1lc3NhZ2UpO1xuICAgICAgfSk7XG4gICAgfSk7XG4gIH1cbn1cbiIsImltcG9ydCB7IE1ldGVvciB9IGZyb20gJ21ldGVvci9tZXRlb3InO1xuaW1wb3J0IHsgRXRoYWNjb3VudHMgfSBmcm9tICcuLi8uLi9jb21tb24vY29sbGVjdGlvbnMvY29sbGVjdGlvbnMnO1xuXG5NZXRlb3IucHVibGlzaCgnZXRoQWNjb3VudHMnLCBmdW5jdGlvbihxdWVyeSA9IHt9KSB7XG4gIHJldHVybiBFdGhhY2NvdW50cy5maW5kKHF1ZXJ5LCB7XG4gICAgZmllbGRzOiB7IF9pZDogMSwgYWRkcmVzczogMSwgdXNlcklkOiAxIH1cbiAgfSk7XG59KTsiLCJpbXBvcnQgeyBNZXRlb3IgfSBmcm9tICdtZXRlb3IvbWV0ZW9yJztcbmltcG9ydCB7IEV0aHRyYW5zZmVycyB9IGZyb20gJy4uLy4uL2NvbW1vbi9jb2xsZWN0aW9ucy9jb2xsZWN0aW9ucyc7XG5cbk1ldGVvci5wdWJsaXNoKCdldGhUcmFuc2ZlcnMnLCBmdW5jdGlvbihxdWVyeSA9IHt9LCBmaWVsZHMgPSB7fSkge1xuICByZXR1cm4gRXRodHJhbnNmZXJzLmZpbmQocXVlcnksIHtcbiAgICBmaWVsZHM6IGZpZWxkc1xuICB9KTtcbn0pOyIsImltcG9ydCB7IE1ldGVvciB9IGZyb20gJ21ldGVvci9tZXRlb3InO1xuXG5NZXRlb3IucHVibGlzaCgndXNlcnNQcm9maWxlJywgZnVuY3Rpb24ocXVlcnkgPSB7fSkge1xuICByZXR1cm4gTWV0ZW9yLnVzZXJzLmZpbmQocXVlcnksIHtcbiAgICBmaWVsZHM6IHsgdXNlcm5hbWU6IDEsIHByb2ZpbGU6IDEgfVxuICB9KTtcbn0pOyIsImltcG9ydCB7IE1ldGVvciB9IGZyb20gXCJtZXRlb3IvbWV0ZW9yXCI7XG5cbmV4cG9ydCBmdW5jdGlvbiBsb2FkQ29uZmlnKCkge1xuICBNZXRlb3Iuc2V0dGluZ3MuQVBQX05BTUUgPSBcIkdyYXZpdHlDb2luXCI7XG4gIE1ldGVvci5zZXR0aW5ncy5UT0tFTl9BRERSRVNTID0gXCIweDBkNGQxNTZiZmEzZmMxZTU1MDhlNTAxNzc0MDAxYmQwZTFmNmM3MjJcIjtcbiAgTWV0ZW9yLnNldHRpbmdzLlRPS0VOX05BTUUgPSBcIkdyYXZpdHlDb2luXCI7XG4gIE1ldGVvci5zZXR0aW5ncy5UT0tFTl9BQkkgPSBgW3tcImNvbnN0YW50XCI6dHJ1ZSxcImlucHV0c1wiOltdLFwibmFtZVwiOlwibmFtZVwiLFwib3V0cHV0c1wiOlt7XCJuYW1lXCI6XCJcIixcInR5cGVcIjpcInN0cmluZ1wifV0sXCJwYXlhYmxlXCI6ZmFsc2UsXCJzdGF0ZU11dGFiaWxpdHlcIjpcInZpZXdcIixcInR5cGVcIjpcImZ1bmN0aW9uXCJ9LHtcImNvbnN0YW50XCI6ZmFsc2UsXCJpbnB1dHNcIjpbe1wibmFtZVwiOlwic3BlbmRlclwiLFwidHlwZVwiOlwiYWRkcmVzc1wifSx7XCJuYW1lXCI6XCJ0b2tlbnNcIixcInR5cGVcIjpcInVpbnQyNTZcIn1dLFwibmFtZVwiOlwiYXBwcm92ZVwiLFwib3V0cHV0c1wiOlt7XCJuYW1lXCI6XCJzdWNjZXNzXCIsXCJ0eXBlXCI6XCJib29sXCJ9XSxcInBheWFibGVcIjpmYWxzZSxcInN0YXRlTXV0YWJpbGl0eVwiOlwibm9ucGF5YWJsZVwiLFwidHlwZVwiOlwiZnVuY3Rpb25cIn0se1wiY29uc3RhbnRcIjp0cnVlLFwiaW5wdXRzXCI6W10sXCJuYW1lXCI6XCJ0b3RhbFN1cHBseVwiLFwib3V0cHV0c1wiOlt7XCJuYW1lXCI6XCJcIixcInR5cGVcIjpcInVpbnQyNTZcIn1dLFwicGF5YWJsZVwiOmZhbHNlLFwic3RhdGVNdXRhYmlsaXR5XCI6XCJ2aWV3XCIsXCJ0eXBlXCI6XCJmdW5jdGlvblwifSx7XCJjb25zdGFudFwiOmZhbHNlLFwiaW5wdXRzXCI6W3tcIm5hbWVcIjpcImZyb21cIixcInR5cGVcIjpcImFkZHJlc3NcIn0se1wibmFtZVwiOlwidG9cIixcInR5cGVcIjpcImFkZHJlc3NcIn0se1wibmFtZVwiOlwidG9rZW5zXCIsXCJ0eXBlXCI6XCJ1aW50MjU2XCJ9XSxcIm5hbWVcIjpcInRyYW5zZmVyRnJvbVwiLFwib3V0cHV0c1wiOlt7XCJuYW1lXCI6XCJzdWNjZXNzXCIsXCJ0eXBlXCI6XCJib29sXCJ9XSxcInBheWFibGVcIjpmYWxzZSxcInN0YXRlTXV0YWJpbGl0eVwiOlwibm9ucGF5YWJsZVwiLFwidHlwZVwiOlwiZnVuY3Rpb25cIn0se1wiY29uc3RhbnRcIjp0cnVlLFwiaW5wdXRzXCI6W10sXCJuYW1lXCI6XCJkZWNpbWFsc1wiLFwib3V0cHV0c1wiOlt7XCJuYW1lXCI6XCJcIixcInR5cGVcIjpcInVpbnQ4XCJ9XSxcInBheWFibGVcIjpmYWxzZSxcInN0YXRlTXV0YWJpbGl0eVwiOlwidmlld1wiLFwidHlwZVwiOlwiZnVuY3Rpb25cIn0se1wiY29uc3RhbnRcIjp0cnVlLFwiaW5wdXRzXCI6W10sXCJuYW1lXCI6XCJfdG90YWxTdXBwbHlcIixcIm91dHB1dHNcIjpbe1wibmFtZVwiOlwiXCIsXCJ0eXBlXCI6XCJ1aW50MjU2XCJ9XSxcInBheWFibGVcIjpmYWxzZSxcInN0YXRlTXV0YWJpbGl0eVwiOlwidmlld1wiLFwidHlwZVwiOlwiZnVuY3Rpb25cIn0se1wiY29uc3RhbnRcIjp0cnVlLFwiaW5wdXRzXCI6W3tcIm5hbWVcIjpcInRva2VuT3duZXJcIixcInR5cGVcIjpcImFkZHJlc3NcIn1dLFwibmFtZVwiOlwiYmFsYW5jZU9mXCIsXCJvdXRwdXRzXCI6W3tcIm5hbWVcIjpcImJhbGFuY2VcIixcInR5cGVcIjpcInVpbnQyNTZcIn1dLFwicGF5YWJsZVwiOmZhbHNlLFwic3RhdGVNdXRhYmlsaXR5XCI6XCJ2aWV3XCIsXCJ0eXBlXCI6XCJmdW5jdGlvblwifSx7XCJjb25zdGFudFwiOmZhbHNlLFwiaW5wdXRzXCI6W10sXCJuYW1lXCI6XCJhY2NlcHRPd25lcnNoaXBcIixcIm91dHB1dHNcIjpbXSxcInBheWFibGVcIjpmYWxzZSxcInN0YXRlTXV0YWJpbGl0eVwiOlwibm9ucGF5YWJsZVwiLFwidHlwZVwiOlwiZnVuY3Rpb25cIn0se1wiY29uc3RhbnRcIjp0cnVlLFwiaW5wdXRzXCI6W10sXCJuYW1lXCI6XCJvd25lclwiLFwib3V0cHV0c1wiOlt7XCJuYW1lXCI6XCJcIixcInR5cGVcIjpcImFkZHJlc3NcIn1dLFwicGF5YWJsZVwiOmZhbHNlLFwic3RhdGVNdXRhYmlsaXR5XCI6XCJ2aWV3XCIsXCJ0eXBlXCI6XCJmdW5jdGlvblwifSx7XCJjb25zdGFudFwiOnRydWUsXCJpbnB1dHNcIjpbXSxcIm5hbWVcIjpcInN5bWJvbFwiLFwib3V0cHV0c1wiOlt7XCJuYW1lXCI6XCJcIixcInR5cGVcIjpcInN0cmluZ1wifV0sXCJwYXlhYmxlXCI6ZmFsc2UsXCJzdGF0ZU11dGFiaWxpdHlcIjpcInZpZXdcIixcInR5cGVcIjpcImZ1bmN0aW9uXCJ9LHtcImNvbnN0YW50XCI6dHJ1ZSxcImlucHV0c1wiOlt7XCJuYW1lXCI6XCJhXCIsXCJ0eXBlXCI6XCJ1aW50MjU2XCJ9LHtcIm5hbWVcIjpcImJcIixcInR5cGVcIjpcInVpbnQyNTZcIn1dLFwibmFtZVwiOlwic2FmZVN1YlwiLFwib3V0cHV0c1wiOlt7XCJuYW1lXCI6XCJjXCIsXCJ0eXBlXCI6XCJ1aW50MjU2XCJ9XSxcInBheWFibGVcIjpmYWxzZSxcInN0YXRlTXV0YWJpbGl0eVwiOlwicHVyZVwiLFwidHlwZVwiOlwiZnVuY3Rpb25cIn0se1wiY29uc3RhbnRcIjpmYWxzZSxcImlucHV0c1wiOlt7XCJuYW1lXCI6XCJ0b1wiLFwidHlwZVwiOlwiYWRkcmVzc1wifSx7XCJuYW1lXCI6XCJ0b2tlbnNcIixcInR5cGVcIjpcInVpbnQyNTZcIn1dLFwibmFtZVwiOlwidHJhbnNmZXJcIixcIm91dHB1dHNcIjpbe1wibmFtZVwiOlwic3VjY2Vzc1wiLFwidHlwZVwiOlwiYm9vbFwifV0sXCJwYXlhYmxlXCI6ZmFsc2UsXCJzdGF0ZU11dGFiaWxpdHlcIjpcIm5vbnBheWFibGVcIixcInR5cGVcIjpcImZ1bmN0aW9uXCJ9LHtcImNvbnN0YW50XCI6dHJ1ZSxcImlucHV0c1wiOlt7XCJuYW1lXCI6XCJhXCIsXCJ0eXBlXCI6XCJ1aW50MjU2XCJ9LHtcIm5hbWVcIjpcImJcIixcInR5cGVcIjpcInVpbnQyNTZcIn1dLFwibmFtZVwiOlwic2FmZURpdlwiLFwib3V0cHV0c1wiOlt7XCJuYW1lXCI6XCJjXCIsXCJ0eXBlXCI6XCJ1aW50MjU2XCJ9XSxcInBheWFibGVcIjpmYWxzZSxcInN0YXRlTXV0YWJpbGl0eVwiOlwicHVyZVwiLFwidHlwZVwiOlwiZnVuY3Rpb25cIn0se1wiY29uc3RhbnRcIjpmYWxzZSxcImlucHV0c1wiOlt7XCJuYW1lXCI6XCJzcGVuZGVyXCIsXCJ0eXBlXCI6XCJhZGRyZXNzXCJ9LHtcIm5hbWVcIjpcInRva2Vuc1wiLFwidHlwZVwiOlwidWludDI1NlwifSx7XCJuYW1lXCI6XCJkYXRhXCIsXCJ0eXBlXCI6XCJieXRlc1wifV0sXCJuYW1lXCI6XCJhcHByb3ZlQW5kQ2FsbFwiLFwib3V0cHV0c1wiOlt7XCJuYW1lXCI6XCJzdWNjZXNzXCIsXCJ0eXBlXCI6XCJib29sXCJ9XSxcInBheWFibGVcIjpmYWxzZSxcInN0YXRlTXV0YWJpbGl0eVwiOlwibm9ucGF5YWJsZVwiLFwidHlwZVwiOlwiZnVuY3Rpb25cIn0se1wiY29uc3RhbnRcIjp0cnVlLFwiaW5wdXRzXCI6W3tcIm5hbWVcIjpcImFcIixcInR5cGVcIjpcInVpbnQyNTZcIn0se1wibmFtZVwiOlwiYlwiLFwidHlwZVwiOlwidWludDI1NlwifV0sXCJuYW1lXCI6XCJzYWZlTXVsXCIsXCJvdXRwdXRzXCI6W3tcIm5hbWVcIjpcImNcIixcInR5cGVcIjpcInVpbnQyNTZcIn1dLFwicGF5YWJsZVwiOmZhbHNlLFwic3RhdGVNdXRhYmlsaXR5XCI6XCJwdXJlXCIsXCJ0eXBlXCI6XCJmdW5jdGlvblwifSx7XCJjb25zdGFudFwiOnRydWUsXCJpbnB1dHNcIjpbXSxcIm5hbWVcIjpcIm5ld093bmVyXCIsXCJvdXRwdXRzXCI6W3tcIm5hbWVcIjpcIlwiLFwidHlwZVwiOlwiYWRkcmVzc1wifV0sXCJwYXlhYmxlXCI6ZmFsc2UsXCJzdGF0ZU11dGFiaWxpdHlcIjpcInZpZXdcIixcInR5cGVcIjpcImZ1bmN0aW9uXCJ9LHtcImNvbnN0YW50XCI6ZmFsc2UsXCJpbnB1dHNcIjpbe1wibmFtZVwiOlwidG9rZW5BZGRyZXNzXCIsXCJ0eXBlXCI6XCJhZGRyZXNzXCJ9LHtcIm5hbWVcIjpcInRva2Vuc1wiLFwidHlwZVwiOlwidWludDI1NlwifV0sXCJuYW1lXCI6XCJ0cmFuc2ZlckFueUVSQzIwVG9rZW5cIixcIm91dHB1dHNcIjpbe1wibmFtZVwiOlwic3VjY2Vzc1wiLFwidHlwZVwiOlwiYm9vbFwifV0sXCJwYXlhYmxlXCI6ZmFsc2UsXCJzdGF0ZU11dGFiaWxpdHlcIjpcIm5vbnBheWFibGVcIixcInR5cGVcIjpcImZ1bmN0aW9uXCJ9LHtcImNvbnN0YW50XCI6dHJ1ZSxcImlucHV0c1wiOlt7XCJuYW1lXCI6XCJ0b2tlbk93bmVyXCIsXCJ0eXBlXCI6XCJhZGRyZXNzXCJ9LHtcIm5hbWVcIjpcInNwZW5kZXJcIixcInR5cGVcIjpcImFkZHJlc3NcIn1dLFwibmFtZVwiOlwiYWxsb3dhbmNlXCIsXCJvdXRwdXRzXCI6W3tcIm5hbWVcIjpcInJlbWFpbmluZ1wiLFwidHlwZVwiOlwidWludDI1NlwifV0sXCJwYXlhYmxlXCI6ZmFsc2UsXCJzdGF0ZU11dGFiaWxpdHlcIjpcInZpZXdcIixcInR5cGVcIjpcImZ1bmN0aW9uXCJ9LHtcImNvbnN0YW50XCI6dHJ1ZSxcImlucHV0c1wiOlt7XCJuYW1lXCI6XCJhXCIsXCJ0eXBlXCI6XCJ1aW50MjU2XCJ9LHtcIm5hbWVcIjpcImJcIixcInR5cGVcIjpcInVpbnQyNTZcIn1dLFwibmFtZVwiOlwic2FmZUFkZFwiLFwib3V0cHV0c1wiOlt7XCJuYW1lXCI6XCJjXCIsXCJ0eXBlXCI6XCJ1aW50MjU2XCJ9XSxcInBheWFibGVcIjpmYWxzZSxcInN0YXRlTXV0YWJpbGl0eVwiOlwicHVyZVwiLFwidHlwZVwiOlwiZnVuY3Rpb25cIn0se1wiY29uc3RhbnRcIjpmYWxzZSxcImlucHV0c1wiOlt7XCJuYW1lXCI6XCJfbmV3T3duZXJcIixcInR5cGVcIjpcImFkZHJlc3NcIn1dLFwibmFtZVwiOlwidHJhbnNmZXJPd25lcnNoaXBcIixcIm91dHB1dHNcIjpbXSxcInBheWFibGVcIjpmYWxzZSxcInN0YXRlTXV0YWJpbGl0eVwiOlwibm9ucGF5YWJsZVwiLFwidHlwZVwiOlwiZnVuY3Rpb25cIn0se1wiaW5wdXRzXCI6W10sXCJwYXlhYmxlXCI6ZmFsc2UsXCJzdGF0ZU11dGFiaWxpdHlcIjpcIm5vbnBheWFibGVcIixcInR5cGVcIjpcImNvbnN0cnVjdG9yXCJ9LHtcInBheWFibGVcIjp0cnVlLFwic3RhdGVNdXRhYmlsaXR5XCI6XCJwYXlhYmxlXCIsXCJ0eXBlXCI6XCJmYWxsYmFja1wifSx7XCJhbm9ueW1vdXNcIjpmYWxzZSxcImlucHV0c1wiOlt7XCJpbmRleGVkXCI6dHJ1ZSxcIm5hbWVcIjpcIl9mcm9tXCIsXCJ0eXBlXCI6XCJhZGRyZXNzXCJ9LHtcImluZGV4ZWRcIjp0cnVlLFwibmFtZVwiOlwiX3RvXCIsXCJ0eXBlXCI6XCJhZGRyZXNzXCJ9XSxcIm5hbWVcIjpcIk93bmVyc2hpcFRyYW5zZmVycmVkXCIsXCJ0eXBlXCI6XCJldmVudFwifSx7XCJhbm9ueW1vdXNcIjpmYWxzZSxcImlucHV0c1wiOlt7XCJpbmRleGVkXCI6dHJ1ZSxcIm5hbWVcIjpcImZyb21cIixcInR5cGVcIjpcImFkZHJlc3NcIn0se1wiaW5kZXhlZFwiOnRydWUsXCJuYW1lXCI6XCJ0b1wiLFwidHlwZVwiOlwiYWRkcmVzc1wifSx7XCJpbmRleGVkXCI6ZmFsc2UsXCJuYW1lXCI6XCJ0b2tlbnNcIixcInR5cGVcIjpcInVpbnQyNTZcIn1dLFwibmFtZVwiOlwiVHJhbnNmZXJcIixcInR5cGVcIjpcImV2ZW50XCJ9LHtcImFub255bW91c1wiOmZhbHNlLFwiaW5wdXRzXCI6W3tcImluZGV4ZWRcIjp0cnVlLFwibmFtZVwiOlwidG9rZW5Pd25lclwiLFwidHlwZVwiOlwiYWRkcmVzc1wifSx7XCJpbmRleGVkXCI6dHJ1ZSxcIm5hbWVcIjpcInNwZW5kZXJcIixcInR5cGVcIjpcImFkZHJlc3NcIn0se1wiaW5kZXhlZFwiOmZhbHNlLFwibmFtZVwiOlwidG9rZW5zXCIsXCJ0eXBlXCI6XCJ1aW50MjU2XCJ9XSxcIm5hbWVcIjpcIkFwcHJvdmFsXCIsXCJ0eXBlXCI6XCJldmVudFwifV1gO1xuICBNZXRlb3Iuc2V0dGluZ3MuTUFTVEVSX0FDQ09VTlQgPSBcIjB4MGRlMmY4Yzk2NTEyNjUyOWM1MjdkMjA1OThmMWFhMTllZTgzYWNlY1wiO1xuICAvLyBNYXN0ZXIgYWNjb3VudCBqdXN0IHdpbGwgbmVlZCBvbmUgb2YgdGhlc2Ugb25lczpcbiAgLy8gLSBLZXlzdG9yZSBkYXRhLlxuICBNZXRlb3Iuc2V0dGluZ3MuTUFTVEVSX0FDQ09VTlRfS0VZU1RPUkVfRklMRSA9IFwiVVRDLS0yMDE4LTA0LTI1VDE0LTU2LTQzLjE0Nzc1MTAwMFotLWM1ZjVmNzEwMzQzMWNjYzE1MzFiOTAwY2Y3MGE3ZGE4NjM1MjI1MjBcIjtcbiAgTWV0ZW9yLnNldHRpbmdzLk1BU1RFUl9BQ0NPVU5UX1BBU1NXT1JEX0ZJTEUgPSBcIlVUQy0tMjAxOC0wNC0yNVQxNC01Ni00My4xNDc3NTEwMDBaLS1jNWY1ZjcxMDM0MzFjY2MxNTMxYjkwMGNmNzBhN2RhODYzNTIyNTIwX3Bhc3N3b3JkXCI7XG4gIC8vIC0gUHJpdmF0ZSBrZXkuXG4gIC8vIE11c3QgYmUgcHJlY2VkZWQgYnkgMHguXG4gIE1ldGVvci5zZXR0aW5ncy5NQVNURVJfQUNDT1VOVF9QUklWQVRFS0VZX0ZJTEUgPSBcIi4uLy4uLy4uL3ByaXZhdGVLZXlzL215UHJpdmF0ZUtleVwiO1xuICBNZXRlb3Iuc2V0dGluZ3MuVE9LRU5fU1lNQk9MID0gXCJHVENcIjtcbiAgTWV0ZW9yLnNldHRpbmdzLk5FVFdPUktfVVJMID0gXCJodHRwczovL3JvcHN0ZW4uaW5mdXJhLmlvXCI7XG4gIC8vIENoYWluSWQgaXMgMyBmb3IgdGhlIHRlc3QgbmV0d29yayByb3BzdGVuLiBcbiAgTWV0ZW9yLnNldHRpbmdzLk5FVFdPUktfQ0hBSU5JRCA9IDM7XG59XG4iLCJpbXBvcnQgeyBsb2FkQ29uZmlnIH0gZnJvbSAnLi4vY29tbW9uL2NvbmZpZyc7XG5cbmV4cG9ydCBmdW5jdGlvbiBsb2FkU2VydmVyQ29uZmlnKGVudikge1xuICBzd2l0Y2ggKGVudikge1xuICAgIGNhc2UgXCJwcm9kdWN0aW9uXCI6XG4gICAgY2FzZSBcInN0YWdpbmdcIjpcbiAgICBjYXNlIFwiZGV2ZWxvcG1lbnRcIjpcbiAgICBkZWZhdWx0OlxuICAgICAgbG9hZENvbmZpZygpO1xuICAgICAgYnJlYWs7XG4gIH1cbn1cbiIsImltcG9ydCB7IE1ldGVvciB9IGZyb20gJ21ldGVvci9tZXRlb3InO1xuaW1wb3J0IHsgbG9hZFNlcnZlckNvbmZpZyB9IGZyb20gJy4vY29uZmlnJztcblxuTWV0ZW9yLnN0YXJ0dXAoKCkgPT4ge1xuICBsZXQgZW52ID0gcHJvY2Vzcy5lbnYuTk9ERV9FTlY7XG4gIGxvYWRTZXJ2ZXJDb25maWcoZW52KTtcbiAgY29uc29sZS5sb2coXCJBTEwgT0shXCIpO1xufSk7XG5cbiIsImltcG9ydCB7IEZsb3dSb3V0ZXIgfSBmcm9tICdtZXRlb3Iva2FkaXJhOmZsb3ctcm91dGVyJztcbmltcG9ydCB7IE1ldGVvciB9IGZyb20gJ21ldGVvci9tZXRlb3InO1xuXG4vLyBTZXQgdXAgYWxsIHJvdXRlcyBpbiB0aGUgYXBwXG5GbG93Um91dGVyLnJvdXRlKCcvJywge1xuICBuYW1lOiAnaG9tZScsXG4gIGFjdGlvbigpIHtcbiAgICBCbGF6ZUxheW91dC5yZW5kZXIoJ2xheW91dERlZmF1bHQnLCB7IGNvbnRlbnQ6ICdob21lJyB9KTtcbiAgfSxcbn0pO1xuXG5GbG93Um91dGVyLnJvdXRlKCcvdXNlci1kZXRhaWxzJywge1xuICBuYW1lOiAndXNlckRldGFpbHMnLFxuICBhY3Rpb24oKSB7XG4gICAgQmxhemVMYXlvdXQucmVuZGVyKCdsYXlvdXREZWZhdWx0JywgeyBjb250ZW50OiAndXNlckRldGFpbHMnIH0pO1xuICB9LFxuICB0cmlnZ2Vyc0VudGVyKCkge1xuICAgIGlmICghTWV0ZW9yLnVzZXJJZCgpKSB7XG4gICAgICBGbG93Um91dGVyLmdvKCdob21lJyk7XG4gICAgfVxuICB9XG59KTtcblxuRmxvd1JvdXRlci5yb3V0ZSgnL3RyYW5zZmVydG8vOnVzZXJJZCcsIHtcbiAgbmFtZTogJ3RyYW5zZmVyVG8nLFxuICBhY3Rpb24oKSB7XG4gICAgQmxhemVMYXlvdXQucmVuZGVyKCdsYXlvdXREZWZhdWx0JywgeyBjb250ZW50OiAndHJhbnNmZXJUbycgfSk7XG4gIH0sXG59KTtcblxuRmxvd1JvdXRlci5yb3V0ZSgnL3RyYW5zZmVyLXJlc3VsdC86dHJhbnNmZXJJZCcsIHtcbiAgbmFtZTogJ3RyYW5zZmVyUmVzdWx0JyxcbiAgYWN0aW9uKCkge1xuICAgIEJsYXplTGF5b3V0LnJlbmRlcignbGF5b3V0RGVmYXVsdCcsIHsgY29udGVudDogJ3RyYW5zZmVyUmVzdWx0JyB9KTtcbiAgfSxcbn0pO1xuXG5GbG93Um91dGVyLnJvdXRlKCcvdHJhbnNmZXJzJywge1xuICBuYW1lOiAndHJhbnNmZXJzJyxcbiAgYWN0aW9uKCkge1xuICAgIEJsYXplTGF5b3V0LnJlbmRlcignbGF5b3V0RGVmYXVsdCcsIHsgY29udGVudDogJ3RyYW5zZmVycycgfSk7XG4gIH0sXG59KTtcblxuRmxvd1JvdXRlci5ub3RGb3VuZCA9IHtcbiAgYWN0aW9uKCkge1xuICAgIEJsYXplTGF5b3V0LnJlbmRlcignbGF5b3V0RGVmYXVsdCcsIHsgY29udGVudDogJ0FwcF9ub3RGb3VuZCcgfSk7XG4gIH0sXG59O1xuXG4vLyBTZXQgdXAgYWxsIHJvdXRlcyBpbiB0aGUgYXBwLlxuRmxvd1JvdXRlci5yb3V0ZSgnL2xvZ2luJywge1xuICBuYW1lOiAnbG9naW4nLFxuICBhY3Rpb24oKSB7XG4gICAgQmxhemVMYXlvdXQucmVuZGVyKCdsYXlvdXREZWZhdWx0JywgeyBjb250ZW50OiAnbG9naW4nIH0pO1xuICB9LFxufSk7XG5cbi8vIFNldCB1cCBhbGwgcm91dGVzIGluIHRoZSBhcHAuXG5GbG93Um91dGVyLnJvdXRlKCcvcmVnaXN0ZXInLCB7XG4gIG5hbWU6ICdyZWdpc3RlcicsXG4gIGFjdGlvbigpIHtcbiAgICBCbGF6ZUxheW91dC5yZW5kZXIoJ2xheW91dERlZmF1bHQnLCB7IGNvbnRlbnQ6ICdyZWdpc3RlcicgfSk7XG4gIH1cbn0pOyIsIi8vIFNlcnZlciBlbnRyeSBwb2ludCwgaW1wb3J0cyBhbGwgc2VydmVyIGNvZGVcblxuaW1wb3J0ICcvaW1wb3J0cy9zdGFydHVwL3NlcnZlcic7XG4iXX0=
